<link rel="stylesheet" media="screen and (min-width: 1150px)" href="style/tv.css">
<link rel="stylesheet" media="screen and (max-width: 1149px)" href="style/tv_sm.css">

<?PHP
	/* change character set to utf8 
		if (!mysqli_set_charset($adminC, "utf8")) {
		printf("Error loading character set utf8: %s\n", mysqli_error($adminC));
		echo "ERRRRRor";
		exit();
		} else {
		printf("Current character set: %s\n", mysqli_character_set_name($adminC));
		echo "EROr";
		}
		
	*/
	
	if(isset($_GET['i'])){
		$link= $_GET['i'];
		$sql="SELECT * FROM `pallapc_main`.`tv` where type='1' and `link`='$link' order by id desc ";
		
		$result=mysqli_query($dbC ,$sql);
		$rowcount=mysqli_num_rows($result);
		if($rowcount==0){ ///if not in the DB go to home ?>
		
		<script language="javascript" type="text/javascript">
			self.location="tv.php";
		</script>
		
		<?PHP
			}else{
			
			while($rows=mysqli_fetch_array($result)){
				$title	=$rows['title'];
				$description=$rows['description'];
				$on_head=$rows['on_head'];
				$more	=$rows['more'];
				$code	=$rows['code'];
				$img	=$rows['img'];
				$img_alt=$rows['img_alt'];
				$img_big=$rows['img_big'];
				$views	=$rows['views'];
				$shareFB=$rows['shareFB'];
				$shareTW=$rows['shareTW'];
				$shareO=$rows['shareO'];
				$like	=$rows['like'];
				$dislike	=$rows['dislike'];
				$head_title	=$rows['head_title'];
				$head_description=$rows['head_description'];
				$datechange	=$rows['datechange'];
				$website	=$rows['website'];
				$freq	=$rows['freq'];
				$id_tv	=$rows['id'];
				$type	=$rows['type'];
				$published=$rows['published'];
				
			}   //end while .. now print the TV channel header
			
			
			$style="<title>".$head_title."</title>
			<meta name='description' content='".$head_description."'>
			<meta property='og:title' content='$head_title'/>
			<meta property='og:description' content='$head_description'/>
			<meta property='og:image' content='$img_big'/>
			<meta property='og:type' content='video.tv_show'/>
			<meta property='og:url' content='https://pallap.com/tv.php?i=$link' />
			";
		}
		
		} else{ //print The main page header
		
		$sql_main="SELECT * FROM `pallapc_main`.`main_pages` where id_main='1' ";
		$result_main=mysqli_query($dbC ,$sql_main);
		while($main=mysqli_fetch_array($result_main)){
			$title_main=$main['title_main'];
			$description_main=$main['description_main'];
			$words_on_main=$main['words_on_main'];
			$views_main=$main['views_main'];
			$img_share_main=$main['img_share_main'];
		}
		
		$style="<meta name='description' content='$description_main'>
		<title>$title_main</title>
		<meta property='og:image' content='$img_share_main'/>
		<link rel='stylesheet' type='text/css' href='style/main_p.css' />
		";
		
		/// UPDATE Views
		$n_views_main=$views_main+1;
		$UPDATE_view_main="UPDATE `pallapc_main`.`main_pages` SET `views_main`='$n_views_main' where `id_main`=1";
		mysqli_query($dbC,$UPDATE_view_main);
		/// END UPDATE Views
	}
?>					