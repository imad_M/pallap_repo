<meta http-equiv="Content-Language" content="ar">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://pallap.com/style/fontawesome/css/all.min.css"  >
<link rel="stylesheet" href="https://pallap.com/style/tv-radio.css"  >
<link rel="stylesheet" href="https://pallap.com/style/theme.min.css" type="text/css" />

<!-- 	Google ads -->
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9155050930639855"
     crossorigin="anonymous"></script>
<!-- 	Google ads -->

<!-- 	Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24143494-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	
	gtag('config', 'UA-24143494-1');
</script>
<!--	END .. Google ... Analytics     -->

<!-- clarity --->
<script type="text/javascript">
    (function(c,l,a,r,i,t,y){
        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
    })(window, document, "clarity", "script", "7w2tmdl37u");
</script>
<!-- clarity End--->