<div id="info" dir="rtl">
	<?PHP  include('ads/Banner728_90_b.php'); ?>
	
	
	<div class="container-fluid" dir="rtl">
		
		<div class="panel panel-default" dir="rtl">
			<div class="panel-body">
				
				<div class="row">
					
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2" dir="ltr" >
						<div class="total-shares">
							<div class="slash"></div>
							<em>
								<?PHP
									$all_shares	=	$shareFB+$shareTW+$shareO;
									echo $all_shares;
								?>
							</em>
							<div class="caption"> Shares </div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" dir="ltr" align="left">
						<div class="share_b">
							<a class="btn btn-facebook" rel="nofollow" href="https://pallap.com/include/share.php?i=<?PHP echo $link; ?>&to=fb&from=radio" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=500');return false;"><i class="fa fa-facebook">&nbsp; &nbsp; </i><b>Share on</b> Facebook &nbsp;<span class="badge"><?PHP echo $shareFB; ?></span></a>
							
							<a class="btn btn-twitter" rel="nofollow" href="https://pallap.com/include/share.php?i=<?PHP echo $link; ?>&to=tw&from=radio" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=600');return false;"><i class="fa fa-twitter"></i>&nbsp; Twitter &nbsp; <span class="badge"><?PHP echo $shareTW; ?></span></a>
							
							
							<a class="btn btn-linkedin" rel="nofollow" href="https://pallap.com/include/share.php?i=<?PHP echo $link; ?>&to=likedin&from=radio" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=600');return false;"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
					
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" dir="rtl">
						<div  class="btn btn-default btn-block">
						مشاهدات &nbsp;  <font size="+2"><?PHP echo $views; ?></font></div>
					</div>
					
				</div>
				
			</div>
		</div>
		
		
		
		
        <div class="row"  dir="rtl">
			<div class="col-xs-12 col-md-3" >
				<h5 align="center">
					<a class="btn btn-danger btn-xs" rel="nofollow" href="https://pallap.com/include/report.php?i=<?PHP echo $link; ?>&from=radio"  onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=500');return false;"><i class="fa fa-exclamation-triangle">  &nbsp; بلغ عن بث لا يعمل</i></a>
					
				</h5>
			</div>
			
			
			
			<div class="col-xs-12  col-sm-4 col-md-3" dir="rtl" >
				<?PHP if($website!=""){  ?>
					<h5 align="center"><a class="btn btn-primary btn-xs" href="<?php echo $website; ?>" target="_blank" rel="nofollow"><i class="glyphicon glyphicon-link"> الموقع الالكتروني للاذاعة </i></a></h5>
				<?PHP } ?>
			</div>
			
			<div class="col-xs-12 col-md-3" dir="rtl" >
				<?PHP if($freq!=""){  ?>
					<h5 align="center"><i class="fa fa-signal"> &nbsp; ترددات  الإذاعة  <?php echo $freq; ?></i></h5>
				<?PHP } ?>
			</div>
			
			<div class="col-xs-12 col-md-3" dir="rtl" >
				<h5 align="center">آخر تحديث للبث: <?php echo $datechange; ?></h5>
			</div>
			
		</div>
		
		
		
		<div class="row" >
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 float-right" dir="rtl" >
			
					<img src="<?PHP echo $img; ?>" alt="<?PHP echo $img_alt; ?>">
			
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" dir="rtl" >
				<div >
					
						<div class="alert alert-info"><p>أنت تستمتع إلى إذاعة  &nbsp; <?PHP echo $title; ?>  &nbsp; </p>
							
								<?PHP if($more!=""){  ?>
									<?PHP echo $more; ?>
								<?PHP } ?>
							
							<div class="alert"><?PHP echo $description; ?></div>

						</div>
					
					
				</div>		
			</div>
			
			
			
		</div>
		
		
		
		
		
		
		
	</div>
</div>
<div id="tools"></div>

</div>
