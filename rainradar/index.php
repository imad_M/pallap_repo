<!DOCTYPE html>
<html lang="ar">
	<head>
		<?php
			include_once '../include/all_header.php';
		?>
		<link rel="stylesheet" href="../style/tv.css" type="text/css" />
        <META NAME="description" CONTENT=" מכם גשם رادار المطر في فلسطين المحتلة الضفة الغربية و الأردن و عمان و غزة رام الله القدس نابلس الخليل طولكرم جنين الخليل Rain Radar il Israel Palestine Jordan راين رادار ">
		<title>رادار المطر في فلسطين Rain Radar Palestine & Jordan & Israel</title>
		<meta property="og:image" content="https://pallap.com/rainradar/radaraimage.gif"/>
		
	</head>
	<body>
		<script type="text/javascript">
			setInterval(function() {
				window.location.reload();
			}, 600000);
		</script>
		<?php
			include_once '../include/all_body.php';
			include_once '../include/nav_bar.php';
		?>
		
		
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12 col-md-12 col-lg-6">
					
					
					<iframe width="98%" height="470"
					src="https://embed.windy.com/embed2.html?lat=31.915&lon=34.816&detailLat=31.780&detailLon=35.230&width=370&height=470&zoom=8&level=surface&overlay=radar&product=radar&menu=&message=&marker=&calendar=now&pressure=&type=map&location=coordinates&detail=&metricWind=km%2Fh&metricTemp=%C2%B0C&radarRange=-1"
					frameborder="0"></iframe>
				</div>
				
				
				<div class="col-sm-12 col-xs-12 col-md-12 col-lg-6">
					<?php   	include_once '../include/ads/Banner_rain.php';   ?>
					
				</div>
			</div>
		</div>
		
		
		<h1>رادار المطر -  Rain radar</h1>
		<h3>فلسطين </h3>
		
		<p> رادار المطر راين رادر صور أقمار صناعية فلسطين المحتلة و الضفة الغربية و  غزة رام الله القدس نابلس الخليل طولكرم جنين الخليل قلقيلية إسرائيل تل أبيب </p>
		<p>מכ"ם גשם
			תנועת גשם באוויר
			תמונות מזג אוויר
			תמונות לוויין
			פלסטין הכבושה
			ישראל
		הגדה המערבית ועזה רמאללה, ירושלים, שכם, חברון, טול כרם, ג'נין, חברון, קלקיליה, תל אביב</p>
		<p>rain radar
			rain movement in the air
			weather pictures
			satellite images
			Occupied Palestine
			Israel
		West Bank and Gaza Ramallah, Jerusalem, Nablus, Hebron, Tulkarm, Jenin, Hebron, Qalqilya, Tel Aviv</p>
		<br />
		<!----   TIME and DATE ----->
		<p id="dnt"></p>                                                                        
		<script>document.getElementById("dnt").innerHTML = Date();	</script>
		<!----   END TIME and DATE ----->
		
		<?PHP
			$footer='on';
			include '../include/all_footer.php';
			
		?>
	</body>
</html>