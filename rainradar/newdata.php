<!DOCTYPE html>
<html  lang="en" dir="ltr" >
<head>
  <meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Radar Satellite Page </title>
      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Cache-control" content="no-cache" max-age="20">
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-156066054-1');
    </script>
  
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='https://ims.gov.il/sites/all/modules/jquery_update/replace/jquery/1.9/jquery.min.js'>\x3C/script>")</script>
<script src="https://ims.gov.il/sites/default/files/js/js_3K0VqgP2MnLCHhuneOzkn8l2NtiUOIsKenatWmfVTvI.js"></script>
<script src="https://ims.gov.il/sites/default/files/js/js_PuIsAg7-jsV2J-t7RSW_BheHe1uQqZPsI8ihLpohLVg.js"></script>
<script src="https://ims.gov.il/sites/default/files/js/js_hDwfusRje1onuOh6st_7N6hL6clK1L5Tlns6GM666BU.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"en\/","ajaxPageState":{"theme":"zen","theme_token":"95PH25ipKXPyy7gbVCwbpWlEON2ez6Nvay4FzWxWyqk","js":{"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.9.1\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/ims_tools\/js\/ims_tools.js":1,"sites\/all\/themes\/zen\/js\/moment-with-locales.js":1,"sites\/all\/themes\/zen\/js\/js.cookie.js":1,"sites\/all\/themes\/zen\/js\/angular.min.js":1,"sites\/all\/themes\/zen\/js\/ims_angular.js":1,"sites\/all\/themes\/zen\/js\/dinamic_maps.js":1,"sites\/all\/themes\/zen\/js\/ims_tools.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/ims_tools\/css\/ims_tools.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/zen\/system.base.css":1,"sites\/all\/themes\/zen\/system.menus.css":1,"sites\/all\/themes\/zen\/system.messages.css":1,"sites\/all\/themes\/zen\/system.theme.css":1,"sites\/all\/themes\/zen\/comment.css":1,"sites\/all\/themes\/zen\/node.css":1}},"urlIsAjaxTrusted":{"\/en\/RadarSatellite":true},"ims_nid":"1443"});</script>
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
<body class="html not-front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-1443 node-type-radar-satellite-page i18n-en section-radarsatellite" >
      <p class="skip-link__wrapper">
      <a href="#main-menu" class="skip-link visually-hidden visually-hidden--focusable" id="skip-link">Jump to navigation</a>
    </p>
      <link rel="stylesheet" href="https://ims.gov.il/sites/all/themes/zen/css/bootstrap.min.css">
<script src="https://ims.gov.il/sites/all/themes/zen/js/jquery-3.3.1.min.js"></script>
<script src="https://ims.gov.il/sites/all/themes/zen/ims/js/popper.min.js"></script>
<script src="https://ims.gov.il/sites/all/themes/zen/ims/js/bootstrap.min.js"></script>
<script src='https://portal.mk-sense.com/aweb?license=038949bb4dd545fd91f2fbb0b4ede039' async></script>
<link rel="stylesheet" href="https://ims.gov.il/sites/all/themes/zen/ims/fontawesome/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://ims.gov.il/sites/all/themes/zen/ims/css/slick-theme.css" />
<link rel="stylesheet" type="text/css" href="https://ims.gov.il/sites/all/themes/zen/ims/css/slick.css" />
<link rel="stylesheet" href="https://ims.gov.il/sites/all/themes/zen/ims/css/style.css">
<div class="imagen_container">
    <div class="imagen_container_image">
        <div class="imagen_image_container">
            <span class="imagen_image_container_icon far fa-times-circle"></span>
            <img src="" alt="">
        </div>
        <div class="image_label"></div>
    </div>
</div>
<div class="col-12 ims_npnm full_container">
    <div class="ims_full_container" ng-app="imsApp" ng-cloak>
        <header ng-controller="imshHeaderC as imsc" class="col-12" id="ims_header">
            <nav class="navbar navbar-expand-lg navbar-light nav-bg col-12">
                <a class="navbar-brand" href="{{imsc.logo_link()}}">
                    <img src="https://ims.gov.il/sites/all/themes/zen/ims/images/logo.png" width="45">
                    <span class="hidden_xs">Israel<br>Meteorological<br>Service</span>
                </a>
                <a class="mobile_warnings_button" href="/{{imsc.translations.warnings_page}}?gid=18">
                    <button class="collapsed fnone header_warnings_button" type="button" data-toggle="collapse" data-target="#menu-toggle" aria-controls="menu-toggle" aria-expanded="false" aria-label="Toggle">
                        <img ng-src="{{imsc.get_warning_src()}}" width="35">
                        <small class="fs-5"><span class="title_label">&nbsp;Warnings</span></small>
                    </button>
                </a>
                <a class="hidden_sm mobile_menu_button" href="">
                    <button class="navbar-toggler-menu" type="button">
                        <i class="navbar-toggler-icon fa fa-bars"></i>
                    </button>
                </a>
                <a class="hidden_sm mobile_menu_search_button" href="">
                    <button class="search-toggler" type="button">
                        <i class="fas fa-search"></i>
                    </button>
                </a>
                <div class="collapse navbar-collapse" id="no-menu-toggle" ng-cloak>
                    <ul class="navbar-nav mt-1 mt-lg-0 ims_main_menu">
                        <li class="nav-item {{imsc.sub_menu(item) ? 'dropdown' : ''}} {{imsc.main_menu_get_active_item(item.link)}}" ng-repeat="(item_index, item) in imsc.main_menu" ng-cloak>
                            <a class="nav-link parent_link {{imsc.sub_menu(item) ? 'dropdown-toggle' : ''}}" href="{{item.link}}">{{item.title}}</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown" ng-show="imsc.sub_menu(item)">
                                <a class="dropdown-item" target="{{imsc.is_link_external(sub.link) ? '_blank' : ''}}" href="{{sub.link}}" ng-repeat="(sub_index, sub) in item.below">{{sub.title}}</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="ims_languages" ng-cloak>
                        <li>{{imsc.fix_language_index(imsc.get_language)}}</li>
                        <li ng-repeat="(item_index, item) in imsc.translations.page">
                            <a class="" href="/{{item}}">{{imsc.fix_language_index(item_index)}}</a>
                        </li>
                        <li class="menu_search"><i class="fas fa-search"></i></li>
                    </ul>
                    <ul class="navbar-nav my-icons warnings_box_desktop ims_show_on_desktop" ng-cloak>
                        <li class="nav-item nav-item-absolute" ng-cloak>
                            <a ng-cloak href="https://ims.gov.il/{{imsc.translations.warnings_page}}" class="{{imsc.warning_counter ? 'absolute-icon' : 'no_warnings_absolute-icon'}}"></a>
                        </li>
                        <li class="my-nav-item warning-txt">
                            <a class="nav-link" href="https://ims.gov.il/{{imsc.translations.warnings_page}}">
                                WARNINGS                            </a>
                        </li>
                        <li class="nav-item f-nav">
                            <a href="https://ims.gov.il/{{imsc.translations.warnings_page}}?gid=18" data-toggle="tooltip" data-placement="top" title="General Public Warnings">
                                <img class="warning_icons_people"  ng-src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/warning_icons_people{{imsc.is_warnings_group_active('18') ? '_selected' : ''}}.svg">
                            </a>
                        </li>
                        <li class="nav-item f-nav">
                            <a href="https://ims.gov.il/{{imsc.translations.warnings_page}}?gid=17" data-toggle="tooltip" data-placement="top" title="Aviation Warnings">
                                <img class="warning_icons_people"  ng-src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/warning_icons_aviation{{imsc.is_warnings_group_active('17') ? '_selected' : ''}}.svg">
                            </a>
                        </li>
                        <li class="nav-item f-nav">
                            <a href="https://ims.gov.il/{{imsc.translations.warnings_page}}?gid=27" data-toggle="tooltip" data-placement="top" title="Seamanship Warnings">
                                <img class="warning_icons_people"  ng-src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/warning_icons_shipping{{imsc.is_warnings_group_active('27') ? '_selected' : ''}}.svg">
                            </a>
                        </li>
                        <li class="nav-item f-nav">
                            <a href="https://ims.gov.il/{{imsc.translations.warnings_page}}?gid=50" data-toggle="tooltip" data-placement="top" title="Agriculture warnings">
                                <img class="warning_icons_people"  ng-src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/warning_icons_farmer{{imsc.is_warnings_group_active('50') ? '_selected' : ''}}.svg">
                            </a>
                        </li>
                    </ul>    
                </div>
            </nav>
            <div id="my-side-nav" class="sidenav">
                <div class="nav-side-menu ims_mobile">
                    <div class="menu-list">
                        <ul id="menu-content" class="menu-content"><li data-toggle="collapse" data-target="#subs-0" class="collapsed">
                            <a href="javascript:;">Forecast<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-0"><li class="">
                                        <a href="https://ims.gov.il/en/Forecasts">General Forecast</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/sites/default/files/ims_data/map_images/c3RainForecast/c3RainForecast.png">Rain Maps COSMO</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/sites/default/files/ims_data/map_images/ecRainForecast/ecRainForecast.png">Rain Maps ECMWF</a>
                                    </li></ul><li data-toggle="collapse" data-target="#subs-1" class="collapsed active">
                            <a href="javascript:;">Measurements<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-1"><li class="active">
                                        <a href="https://ims.gov.il/en/RadarSatellite">Radar & Satellite</a>
                                    </li><li class="">
                                        <a href="https://old.govmap.gov.il/app10">Hourly Observations</a>
                                    </li><li class="">
                                        <a href="https://ims.data.gov.il/sites/default/files/sumrain_today.pdf">Today Rain</a>
                                    </li><li class="">
                                        <a href="https://ims.data.gov.il/sites/default/files/sumrain_yesterday.pdf">Yesterday Rain</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/sites/default/files/ims_data/map_images/dailyRainfall/dailyRainfall.pdf">Daily Rainfall Report</a>
                                    </li></ul><li>
                        <a href="/en/Aviation">Aviation</a>
                    </li><li data-toggle="collapse" data-target="#subs-3" class="collapsed">
                            <a href="javascript:;">Marine<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-3"><li class="">
                                        <a href="https://ims.gov.il/en/coasts">Israel Coasts Forecast</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/meditSea">East Mediterranean Sea Forecast</a>
                                    </li><li class="">
                                        <a href="http://asp.mot.gov.il/en/shipping/synoptic-maps">Synoptic Maps & Waves Maps</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/node/1369">Explanation On Sea Waves Height</a>
                                    </li></ul><li data-toggle="collapse" data-target="#subs-4" class="collapsed">
                            <a href="javascript:;">Agriculture<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-4"><li class="">
                                        <a href="https://ims.gov.il/en/evaporation">Evaporation</a>
                                    </li></ul><li data-toggle="collapse" data-target="#subs-5" class="collapsed">
                            <a href="javascript:;">Climate<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-5"><li class="">
                                        <a href="https://ims.gov.il/en/ClimateAtlas">Atlas</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/ClimateReports">Climate Reports</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/Stations">IMS Stations</a>
                                    </li></ul><li data-toggle="collapse" data-target="#subs-6" class="collapsed">
                            <a href="javascript:;">Data Access<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-6"><li class="">
                                        <a href="https://ims.data.gov.il/ims/1">Climate Archive</a>
                                    </li><li class="">
                                        <a href="https://ims.data.gov.il/ims/7">10-minutes Data Archive</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/node/91">10-minutes Data (API)</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/node/1387">Radar & Models Products</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/node/123">Recent Observations & Forecasts (XML)</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/RSS_ForecastAlerts">Recent Forecasts & Alerts (RSS)</a>
                                    </li><li class="">
                                        <a href="http://www.iacdc.tau.ac.il/data-access/datasets/ims/">IMS Database at TAU (IACDC)</a>
                                    </li></ul><li data-toggle="collapse" data-target="#subs-7" class="collapsed">
                            <a href="javascript:;">Outreach<i class="fas fa-caret-down"></i></a> 
                        </li>
                        <ul class="sub-menu collapse" id="subs-7"><li class="">
                                        <a href="https://ims.gov.il/en/AllArticles">Whats New?</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/Clouds">Cloud Atlas</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/QandA">Questions & Answers</a>
                                    </li><li class="">
                                        <a href="https://ims.gov.il/en/node/446">Regional Training Center</a>
                                    </li></ul><li>
                        <a href="/en/about">About</a>
                    </li><li data-toggle="collapse" data-target="#translations" class="collapsed mobile_translations">
                    <div class="row ims_npnm"><a>en</a></div><div class="row ims_npnm item_lng"><a class="ims_active" href="https://ims.gov.il/he/node/1444">עב</a></div></li></ul>                  </div>
                </div>
            </div>
        </header>

        <div class="ims_search col-12 row">
            <form action="https://ims.gov.il/en/RadarSatellite" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-tJzCmVOmmmnxx3HMx1bHtgIx-lMumDMJTD09xlbXPiE" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>        </div>
        <div ng-controller="imsMessageC as mc">
            <div class="col-12 ims_message alert-{{mc.message_data.messages[0].type}} alert alert-dismissible fade show" role="alert" ng-if="mc.message_data.messages[0].description != undefind">
                <div class="ims_message_container col-12 row ims_npnm">

                    <div class="ims_message_container_text  col-12 ims_npnm">

                        <button class="ims_message_button" type="button" class="close" data-open="true" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <span class="ims_message_title">{{mc.message_data.messages[0].title}}: </span><span class="ims_message_message">{{mc.message_data.messages[0].description}}</span>
                    </div>
                </div>
            </div>
        </div>
        


<div class="page_radar_satellite" id="page_radar_satellite" ng-controller="seaController as seaC">
	<div class="ims_base col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 ims_npnm">
		<div ng-controller="radarSatelliteC as rsc" class="satellite_radar" id="satellite_radar" data-satellite-radar-type="{{rsc.radar_satellite_default}}" data-satellite-type="MIDDLE-EAST" data-auto-play="1" data-item-index="{{rsc.radar_satellite_default_lenght-1}}" data-items="{{rsc.radar_satellite_default_lenght}}">
    <a href="https://ims.gov.il/{{rsc.translations.radar_satellite}}"><span class="radar_satellite_full_box fas fa-arrows-alt" aria-hidden="true" data-open="false"></span></a>
    <p class="light-blue-bg">
        <span class="satellite_radar_type {{rsc.radar_satellite_default=='radar' ? 'active' : ''}}" data-type="radar">Rain</span> |
        <span class="satellite_radar_type {{rsc.radar_satellite_default=='IMSRadar' ? 'active' : ''}}" data-type="IMSRadar">Radar</span> |
        <span class="satellite_radar_type {{rsc.radar_satellite_default=='MIDDLE-EAST' ? 'active' : ''}}" data-type="MIDDLE-EAST">Satellite</span>
    </p>
    <p class="col-12 text-center satellite_type_select"><span ng-show="rsc.show_satellite_type_select()"><span class="satellite_type active" data-type="MIDDLE-EAST">Middle East</span> | <span class="satellite_type" data-type="EUROPE">Europe</span></span></p>
    <div class="col-sm-12 photo-3">
        <img class="max-width radar_satellite" style="{{rsc.radar_satellite_default=='uv' ? 'height: 78%; width: auto;' : ''}}" ng-src="{{rsc.radar_satellite_item.file_name}}">
        <p class="col-12 pull-left text-center mt-2">{{rsc.setDate(rsc.radar_satellite_item.forecast_time, 7)}} <span class="items_counter">({{rsc.radar_satellite_item_index+1}}/{{rsc.radar_satellite_lenght}})</span></p>
        <div class="player ims_no_copy">
            <div class="col-12 no-padding">
                <div class="col-sm-10 offset-sm-1 pull-left no-padding">
                    <div class="col-sm-3 satellite_radar_controler previous" data-type="previous">
                        <img src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/player_icons_preview.svg" width="20">
                    </div>
                    <div class="col-sm-3 satellite_radar_controler active pause" data-type="pause">
                        <img src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/player_icons_pause.svg" width="20">
                    </div>
                    <div class="col-sm-3 satellite_radar_controler play" data-type="play">
                        <img src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/player_icons_play.svg" width="20">
                    </div>
                    <div class="col-sm-3 satellite_radar_controler next" data-type="next">
                        <img src="https://ims.gov.il/sites/all/themes/zen/ims/svgs/player_icons_next.svg" width="20">
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>	</div>
</div>        <footer ng-controller="imsFooterC as imsf" class="col-12">
            <div class="row no-margin col-12">
                <div class="col-sm-12 col-12 no-padding footer_menu_logos">
                    <div class="footer_menu_logos_inner row">
                        <div ng-repeat="(item_index, item) in imsf.footer.footer_menu_logos" class="footer_menu_logos_item">
                            <a target="_blank" href="{{item.link}}">
                                <div class="logo_image">
                                    <img ng-src="{{item.logo}}" alt="{{item.title}}" title="{{item.title}}"/>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-8 footer_menu_container ims_npnm">
                    <div class="col-sm-12 col-12 no-padding footer_menu row ims_npnm" id="accordion">
                        <div ng-repeat="(item_index, item) in imsf.footer.footer_menu" class="ims_npnm col-12 col-sm col-lg col-xl ims_col">
                            <span class="col-12 main_footer_item ims_npnm" >{{item.title}}</span>
                            <ul class="collapse" id="ul-1">
                                <li ng-repeat="(sub_index, sub) in item.below">
                                    <a target="{{imsf.is_link_external(sub.link) ? '_blank' : ''}}" href="{{sub.link}}">{{sub.title}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <a target="_blank" href="https://www.gov.il/" class="ims_gov_il_link">
                        <img src="https://ims.gov.il/sites/default/files/Israel_Blue_0.png" alt="rr" />
                    </a>
                </div>
            </div>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('footer a[data-toggle="collapse"]').click(function(e){
      if ($(window).width() >= 576) {
        e.preventDefault();
        e.stopPropagation();
      }    
    });
    $(document).ready(function(e) {
        $('.responsive .col-sm-2').click(function(e) {
            $('.selected-tab').removeClass('selected-tab');
            $(this).addClass('selected-tab');
        });

        $(".ims_colorbox img").before('<i class="ims_expand fas fa-search-plus" aria-hidden="true"></i>');
        $("#my-side-nav .sub-menu li.active").parents(".sub-menu").prev().children().click();
    });
        
    if ($(window).width() <= 1024 && $(".ims_home_page").length){
        var slidesToShow = 1;
        if($(window).width() <= 767){
            slidesToShow = 2;
        }
        else{
            slidesToShow = 4;
        }
        var rtl = $(".i18n-en").length ? false : true;
        $('.forecast_days').slick({
            slidesToShow: slidesToShow,
            slidesToScroll: 1,
            autoplaySpeed: 2000,
            rtl: rtl,
            centerMode: false,
            infinite: false
        });
    }
    function OpenNav() {
        document.getElementById("my-side-nav").style.width = "250px";
    }
    function CloseNav() {
        document.getElementById("my-side-nav").style.width = "0";
    }
</script>  </body>
</html>
