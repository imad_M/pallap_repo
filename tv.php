<?php
	session_start();
	include('include/config.php');
?>
<!DOCTYPE html>
<html lang="ar">
	<head>
		<?php
			include_once('include/tv_header.php');  //code in all TV channel header
			include_once("include/all_header.php"); //code in all the website header
			include_once("include/all_live.php");   //code in live pages (TV & Radio) header
			
			echo $style;   //print header Tages
			echo $on_head;  //more code in header for specefic channel
		?>
	</head>
	
	<body>
		<?PHP
			if(isset($_GET['i'])){  //random channels bar at top
				include_once("include/tv_tape_on_top.php");
			}
		?>
		<div class="wrap">
			<div class="ribbon"></div>
			<div class="main">
				<?PHP  include('include/nav_bar.php'); ?>
				
				
				<div class="content">
					<?PHP
						if(isset($_GET['i'])){ 	//////start channel code ////////
						?>
						
						<div id="info" dir="rtl">
                        	<div class="title" align="center">
								<h1><?PHP echo $head_title; ?></h1>
							</div>
						</div>
						
						
                        <div class="row" >
							
							
							<div class="col-xs-12  col-md-12"><div class="back_video">
								<div id="video1" class="video">
									<div align="center" id="live_code" >
										<?PHP  echo $code; ?>
									</div>
									
								</div>
							</div>
							</div>
							
							
						</div> <!-- End row comments & video-->
						
						
						
						<!--------------------- info TV   ------------------------>
						<?PHP include('include/tv_info.php'); ?>
						<!--------------------- info TV   ------------------------>
						
					</div>
					
					<?PHP
						// UPDATE Views
						$n_views=$views+1;
						$UPDATE_view="UPDATE `pallapc_main`.`tv` SET `views`='$n_views' where `link`='$link'";
						mysqli_query($dbC,$UPDATE_view);
						// END UPDATE Views
						
						
						}else{
						//////End channel code ////////
						include('include/tv_main.php');
					} ?>
					
			</div>   <!-- end main class  -->
			
			
			<?PHP
				include_once 'include/footer.php';
				include_once 'include/all_body.php'; //code in all the website body
			?>
			
			
		</body></html>						