<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-24143494-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>

		<link rel="stylesheet" type="text/css" href="../include/css3_3_1.css">
		
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		
		<title>صفحة إبدأ  | PallaP | </title>
		<META NAME="keywords" CONTENT=" دليل فلسطين الالكتروني ، إلكتروني ، شامل ، صفحة ، البداية ، ابدا ، إبدأ ، إبدأ ، صفحه ابدأ ، أبدأ، صفحة إبدأ ، مواقع ، رائعة، دليل ، صفحه ابدأ ، صفحة ابدأ ، بال لاب، خدمة، خدمات ، مدونة، فلسطين ، دليل فلسطين ">
		<META NAME="description" CONTENT="   دليل فلسطين الالكتروني الشامل وكل ما تريده من مواقع تهمك في صفحة واحدة إبدأ  ابدأ ابدا أبدأ الفلسطينية افضل المواقع  إحدى خدمات شبكة بال لاب مخصصة لتكون افضل صفحة بداية ">
		<meta name="Headline" content="   صفحة ابدأ كل ما تحتاجه في صفحة واحدة دليل مواقع   ببال لاب لرفع الصور والخدمات" />
		
		
		<style type="text/css">
        .header1 {
        padding-bottom: 5px;
        border-bottom: 1px solid #eee;
		     }
			a:link {
			color:#000066;
			font-family: Arial, Helvetica, sans-serif;
			font-style: normal;
			font-weight: normal;
			text-decoration: none;
			font-size: 20px;
			}
			a:visited {
			color:#333333;
			text-decoration: none;
			font-size: 18px;
			}
			a:hover {
			font:bold;
			background-color:#9F3;
			}
			
			.style1 {
			color: #999999; 
			font-family: Arial, Helvetica, sans-serif;
			font-style: normal;
			font-weight: bold;
			}
			.style2 {
			color: #000000;
			font-weight: bold;
			font-size: 25px;
			}
			.style3 {
			font-family: Arial, "Times New Roman", Times, serif;
			font-weight: bold;
			}
			
			.circle {
			background-color:#FCECED;
			height:auto;
			width:200px;
			-moz-border-radius: 25px 0px / 25px 0px;
			border-radius: 25px 0px / 25px 0px;       
			display: inline-block;
			margin: 20px;
			opacity: 0.8;
			-webkit-transition: all 1s ease-out;
			-webkit-transition-delay: 300ms;
			}
			.circle:nth-child(4n+1) {
			background: #CCFF9D;
			height: auto;
			width:190px;
			-moz-border-radius: 35px 5px / 35px 5px;
			border-radius: 35px 5px / 35px 5px; 
			margin: 30px;
			-webkit-transition-delay: 300ms;
			}
			
			
			.circle:hover {
			background-color:#FFF;
			}
			
			.initialState .circle {
			opacity: 0;
			-webkit-transform: scale(0.2);
			}
			
			
			.sug {
			background-color: rgba(214, 255, 176, 1);
			height:150px;
			width: 70px;
			-moz-border-radius: 150px 0 0 150px;
			border-radius: 0px 150px 150px 0px;
			
			margin: 0px;
			font: 0.875em/1 Arial, sans-serif;
			position: fixed;
			left: 0;
			top: 40%;
			background-position: center; 
			
			}
			.sug a{
			position:relative; 
			top: 60px;
			left:-30px;
			}
			
			
			#social_in {
			float:left;
			width:40px;
			height:40px;
			margin: 0px 20px 0 0;
			}
			
			#social_in a {
			display:block;
			width:44px;
			height:44px;
			overflow:hidden;
			border:1px solid transparent;
			line-height:44px;
			text-decoration:none;
			/* css3 */
			/* text-shadow:0 -1px 0 rgba(0,0,0,0.5);   */
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px; /* standards version last */
			}
			
			#social_in a:hover,
			#social_in a:focus,
			#social_in a:active {
			opacity:0.7;
			border-color:#000;
			}
			
			
			/* ------------------------------------------------------------------------------------
			--  FACEBOOK
			------------------------------------------------------------------------------------ */
			
			.facebook a {
			position:relative;
			border-color:#3c5a98;

			text-transform:lowercase;
			text-indent:-5px;
			letter-spacing:10px;
			font-weight:bold;
			font-size:44px;
			line-height:46px;
			color:#fff;
			background:#3c5a98;
			
			/* css3 */
			-moz-box-shadow:0 0 4px rgba(0,0,0,0.4);
			-webkit-box-shadow:0 0 4px rgba(0,0,0,0.4);
			box-shadow:0 0 4px rgba(0,0,0,0.4); /* standards version last */
			}
			
			
			/* ------------------------------------------------------------------------------------
			--  TWITTER
			------------------------------------------------------------------------------------ */
			
			.twitter a {
			position:relative;
			border-color:#a8eaec;
			text-transform:lowercase;
			text-indent:-5px;
			letter-spacing:20px;
			font:bold 40px/1 Tahoma, sans-serif;
			line-height:40px;
			color:#76DDF8;
			background:#daf6f7;
			
			/* css3 */
			text-shadow: 3px 3px 1px #fff, -3px -3px 1px #fff, 3px -3px 1px #fff, -3px 3px 1px #fff;
			-moz-box-shadow:0 0 4px rgba(0,0,0,0.4);
			-webkit-box-shadow:0 0 4px rgba(0,0,0,0.4);
			box-shadow:0 0 4px rgba(0,0,0,0.4); /* standards version last */
			/* NOTE: webkit gradient implementation is not as per spec */
			background:-webkit-gradient(linear, left top, left bottom, from(#dbf7f8), to(#88e1e6));
			background:-moz-linear-gradient(top, #dbf7f8, #88e1e6);
			}
			
			
			
			
			
		</style>
		<script>
			function startIntro() {
				document.body.className = '';
			}
		</script>
	</head>
	
	<body dir="rtl" bgcolor="#333333">
		
        <div class="sug"> <a href="http://www.pallap.com/m/contact-us/" target="_blank" title="إقترح إضافة موقع للصفحة">إقترح<br> موقع</a> </div>
		
		<table   width="95%" align="center"  >
			<tr><td>
				
				
				
				
				
				
				<div   class="header1" dir="rtl" >
					<div class="facebook" id="social_in"><a href="http://www.facebook.com/pallap2" title="صفحتنا على الفيسبوك" target="_blank" rel="nofollow">F</a></div>
					<div class="twitter" id="social_in"><a href="http://twitter.com/pallap1" title="صفحتنا على تويتر" target="_blank" rel="nofollow">T</a></div>
					

                    
					<h3> صفحة إبدأ </h3>
					
					
					
					
					
				</div>
				
				
				
				
				
				
				
				
				
				
			</td>
			</tr>
			<tr>
				<td> <div id="container">
					<div class="circle"><div class="style3">  مواقع عـالـمـيــة  </div>
						<ul>
							<li><a href="http://www.google.com/" title="جـوجــل" target="_blank" rel="nofollow"><b>Google</b></a></li>
							<li><a href="http://www.facebook.com/" title="حسابك في الفيسبوك" target="_blank" rel="nofollow"><b>FaceBook</b></a></li>
							<li><a href="http://www.youtube.com/" title="يوتيوب" target="_blank" rel="nofollow">YouTube</a></li>
							<li><a href="http://www.twitter.com/" title="تويتر" target="_blank" rel="nofollow">Twitter</a></li>
							<li><a href="http://www.linkedin.com/" title="لنكد ان" target="_blank" rel="nofollow">Linkedin</a></li>
							
							<li><a href="http://www.yahoo.com/" title="ياهو" target="_blank" rel="nofollow">!YahoO</a></li>
							
							<li><a href="http://ar.wikipedia.org/" title="wikipedia" target="_blank" rel="nofollow">موسوعة ويكيبيديا</a></li>
							<li><a href="http://www.outlook.com/" title="الدخول الى  ايميل" target="_blank" rel="nofollow">Outlook</a></li>
							<li><a href="http://www.about.com" title="اباوت" target="_blank" rel="nofollow">About.com</a></li>
							<li><a href="http://soundcloud.com/" title="soundcloud" target="_blank" rel="nofollow">Soundcloud</a></li>
                            
<li><a href="https://www.flickr.com" title="فليكر" target="_blank" rel="nofollow">فلكر</a></li>                            
						</li>
					</ul></div> 
					
					
					
					
					<div class="circle"><div class="style3"> أخـبـار - News </div>
						<ul>
							
							
							<li><a href="http://aljazeera.net/" title="موقع قناة الجزيرة" target="_blank" rel="nofollow">الجزيرة نت</a></li>
							<li><a href="http://www.paltimes.net" title="فلسطين الآن | بوابتك إلى الحقيقة" target="_blank">فلسطين الآن</a></li>
							<li><a href="http://maannews.net" title="وكالة معاً الاخبارية" target="_blank" rel="nofollow">وكالة معا</a></li>
							<li><a href="http://safa.ps" title="وكالة االصحافة الفلسطينية - صفا" target="_blank" rel="nofollow">وكالة صفا</a></li>
							<li><a href="http://paltoday.ps" title="فلسطين اليوم" target="_blank" rel="nofollow">وكالة فلسطين اليوم </a></li>
							<li><a href="http://felesteen.ps/" title="موقع صحيفة فلسطين" target="_blank"> فلسطين أون لاين</a></li>
							<li><a href="http://www.islammemo.cc" title="شركة مفكرة الإسلام" target="_blank" rel="nofollow">مفكرة الإسلام</a></li>
							<li><a href="http://www.islam-online.net" title="islam online" target="_blank" >إسلام أون لاين</a></li>
							<li><a href="http://www.almokhtsar.com" title="موقع المختصر للأخبار" target="_blank" rel="nofollow">المختصر </a></li>
							<li><a href="http://almoslim.net" title="موقع المسلم" target="_blank">موقع المـسلم</a></li>

							<li><a href="http://aitnews.com" title=" البوابة العربية للأخبار التقنية " target="_blank" rel="nofollow"> البوابة العربية للأخبار التقنية</a></li>
							
						</ul></div>
						
						
						
						<div class="circle"><div class="style3">  مواقع تـهـمـــك </div>
							<ul>
								<li><a href="http://www.mp3quran.net/" title="المكتبة الصوتية للقرآن الكريم mp3 تضم عدد كبير من القراء و بعدة روايات و بعدة لغات , مع روابط تحميل مباشرة " target="_blank">القرآن الكريم mp3</a></li>
								<li><a href="http://www.islamway.com" title="موقع طريق الاسلام" target="_blank">طريق الإسلام</a></li>    
								<li><a href="http://www.saaid.net/" title="موقع صيد الفوائد" target="_blank">صيد الفوائد</a></li>
								<li><a href="http://www.wathakker.info" title="موقع وذكّر" target="_blank">موقع وذكـر</a></li>
								<li><a href="http://www.islamstory.com" title="موقع قصة الاسلام بإشراف الدكتور راغب السرجاني" target="_blank">قـصـة الإسـلام</a></li>
								<li><a href="http://www.islamicfinder.org/index.php?lang=arabic" title="IslamicFinder" target="_blank">الباحث الاسلامي</a></li>
								<li><a href="http://www.islamweb.net/" title="موقع الشبكة الإسلامية" target="_blank">إسلام ويب</a></li>
								<li><a href="http://www.emanway.com/" title="موقع طريق الايمان" target="_blank" rel="nofollow">طريق الايمان</a></li>
								<li><a href="http://www.dorar.net/" title="لفحص صحة أي حديث " target="_blank" >الدرر السنية</a></li>
								<li><a href="http://www.quran-m.com" title="موسوعة الاعجاز العلمي في القرآن والسنة" target="_blank">موسوعة الاعجاز العلمي</a></li>
								
								
								
								
							</li>
						</ul> </div>        
						
						
						<div class="circle"><div class="style3">  مـنـتـديات ومنوعات </div>
							<ul>
								<li><a href="https://translate.google.com" title="ترجمة جوجل" target="_blank" rel="nofollow"> ترجمة - جوجل</a></li>
								<li><a href="http://www.paldf.net/" title="اضخم منتدى فلسطيني" target="_blank">شبكة فلسطين للحوار</a></li>
								<li><a href="http://www.palestineremembered.com" title="موقع فلسطين في الذاكرة يحوي معلومات عن جميع المدن والقرى الفلسطينية" target="_blank" rel="nofollow">فلسطين في الذاكرة</a></li>
								<li><a href="http://www.multka.net/" title="منتديات الملتقى التربوي" target="_blank" rel="nofollow">الملتقى التربوي</a></li>
								<li><a href="http://www.pal-stu.com/" title="منتديات ملتقى طلاب فلسطين" target="_blank">ملتقى طلاب فلسطين</a></li>
								<li><a href="http://inshad.com/" title="النشيد بلون جديد ، شبكة إنشادكم العالمية المصدر الأول للنشيد و الفن الهادف" target="_blank" rel="nofollow">شبكة إنشادكم العالمية </a></li>
								<li><a href="http://akhawat.islamway.com/forum" title="اضخم منتدى للأخوات إسلامي" target="_blank">اخوات طريق الإسلام</a></li>
								<li><a href="http://p-weather.ps/" title="طقس فلسطين" target="_blank" rel="nofollow">طقس الوطن - فلسطين</a> </li>
                                <li><a href="http://palweather.ps" title="طقس فلسطين" target="_blank" rel="nofollow">طقس فلسطين</a> </li>
								<li><a href="http://www.kooora.com/" title="اضخم موقع رياضي عربي" target="_blank" rel="nofollow">كوووورة</a></li>
								<li><a href="http://www.hihi2.com/" title="موقع رياضي متخصص في اخبار كرة القدم العربية والعالمية
								" target="_blank" rel="nofollow"> هاي كورة</a></li>
								
<li><a href="http://pallap.com/m/2010/03/speed-net-test" title="سرعة اتصالك بالإنترنت الحقيقية" target="_blank">سرعة الإنترنت الصحيحة</a></li>								
							</li>
						</ul> </div>    </div>   	
						
						
						
						<p></p>
						
						
						<p></p>
						<?PHP 
							$footer='on'; 
						include '../include/main_footer.php'; ?>
						
	</div></td>
</tr>
</table>





</body>
</html>