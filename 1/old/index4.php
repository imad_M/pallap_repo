<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-24143494-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>


<link rel="stylesheet" href="../../style/theme.min.css" type="text/css" />

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
   
    <link href="http://noizwaves.github.io/bootstrap-social-buttons/stylesheets/social-buttons.css" rel="stylesheet">

		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<title>صفحة إبدأ  | PallaP |</title>
		<META NAME="keywords" CONTENT=" دليل فلسطين الالكتروني ، إلكتروني ، شامل ، صفحة ، البداية ، ابدا ، إبدأ ، إبدأ ، صفحه ابدأ ، أبدأ، صفحة إبدأ ، مواقع ، رائعة، دليل ، صفحه ابدأ ، صفحة ابدأ ، بال لاب، خدمة، خدمات ، مدونة، فلسطين ، دليل فلسطين ">
		<META NAME="description" CONTENT="   دليل فلسطين الالكتروني الشامل وكل ما تريده من مواقع تهمك في صفحة واحدة إبدأ  ابدأ ابدا أبدأ الفلسطينية افضل المواقع  إحدى خدمات شبكة بال لاب مخصصة لتكون افضل صفحة بداية ">
		<meta name="Headline" content="   صفحة ابدأ كل ما تحتاجه في صفحة واحدة دليل مواقع   ببال لاب لرفع الصور والخدمات" />
		<style type="text/css">
			@font-face {
				font-family: KufiArabic;
				src: url(/fonts/KufiArabic-Regular.ttf);
			}
			a{
			font-size:12px;
			}
		.panel-body li a{
			font-size:14px;
			line-height:33px;
		}
		

			.panel-body li a:hover{
		font-size: 20px;
background-color: rgb(252, 248, 227);
text-decoration: none;
			line-height:33px;


			}
           .logo{
                  background-color: #50147A;
                  border-bottom-left-radius: 33px;
                  border-bottom-right-radius: 33px;
                  width: 100%;
            }
.btn-facebook{color:#fff;background-color:#3b5998;border-color:rgba(0,0,0,0.2)}.btn-facebook:hover,.btn-facebook:focus,.btn-facebook:active,.btn-facebook.active,.open .dropdown-toggle.btn-facebook{color:#fff;background-color:#30487b;border-color:rgba(0,0,0,0.2)}
.btn-facebook:active,.btn-facebook.active,.open .dropdown-toggle.btn-facebook{background-image:none}
.btn-facebook.disabled,.btn-facebook[disabled],fieldset[disabled] .btn-facebook,.btn-facebook.disabled:hover,.btn-facebook[disabled]:hover,fieldset[disabled] .btn-facebook:hover,.btn-facebook.disabled:focus,.btn-facebook[disabled]:focus,fieldset[disabled] .btn-facebook:focus,.btn-facebook.disabled:active,.btn-facebook[disabled]:active,fieldset[disabled] .btn-facebook:active,.btn-facebook.disabled.active,.btn-facebook[disabled].active,fieldset[disabled] .btn-facebook.active{background-color:#3b5998;border-color:rgba(0,0,0,0.2)}

.btn-twitter{color:#fff;background-color:#2ba9e1;border-color:rgba(0,0,0,0.2)}.btn-twitter:hover,.btn-twitter:focus,.btn-twitter:active,.btn-twitter.active,.open .dropdown-toggle.btn-twitter{color:#fff;background-color:#1c92c7;border-color:rgba(0,0,0,0.2)}
.btn-twitter:active,.btn-twitter.active,.open .dropdown-toggle.btn-twitter{background-image:none}
.btn-twitter.disabled,.btn-twitter[disabled],fieldset[disabled] .btn-twitter,.btn-twitter.disabled:hover,.btn-twitter[disabled]:hover,fieldset[disabled] .btn-twitter:hover,.btn-twitter.disabled:focus,.btn-twitter[disabled]:focus,fieldset[disabled] .btn-twitter:focus,.btn-twitter.disabled:active,.btn-twitter[disabled]:active,fieldset[disabled] .btn-twitter:active,.btn-twitter.disabled.active,.btn-twitter[disabled].active,fieldset[disabled] .btn-twitter.active{background-color:#2ba9e1;border-color:rgba(0,0,0,0.2)}

		</style>
	</head>
	
	<body dir="rtl" bgcolor="#333333">
		
		<table   width="95%" align="center"  >
			<tr>
				<td>
					
					<div   class="header1" dir="rtl" >
						<div class="row" dir="rtl">
							
							<div class="col-xs-6 col-sm-4"><div class="text-left">

								<a  class="btn btn-facebook" href="http://www.facebook.com/pallap2"title="صفحتنا على الفيسبوك" target="_blank" rel="nofollow">
									<i class="fa fa-facebook "></i>
								</a>
								
								<a  class="btn btn-twitter" href="http://twitter.com/pallap1" title="صفحتنا على تويتر" target="_blank" rel="nofollow">
									<i class="fa fa-twitter"></i>
								</a></div>
							</div>
                            <div class="col-xs-6 col-sm-4"  >
                            <div class="logo"><center><a href="/" title="الى الرئيسية"> <img id="logo" src="../up/logo-color.png" alt="pallap بال لاب" /></a>  </center> </div>
                            </div>
                            
							<div class="col-xs-6 col-sm-4"><div class="text-right text-muted"><h3> صفحة إبدأ  - <a  class="btn" href="http://pallap.com/m/contact-us/" target="_blank" title="إقترح إضافة موقع للصفحة">إقترح موقع</a> </h3></div></div>
							
						</div>			
						
						
					</div></td>
			</tr>
			<tr>
				<td><div id="container">
					<div class="row" dir="rtl" style="font-family:Tahoma;">
						
						
						<div class="col-xs-6 col-sm-1" ></div>

						<div class="col-xs-6 col-sm-3">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">مـنـتـديات ومنوعات</h3>
								</div>
								<div class="panel-body">
									<ul>
										<li><a href="https://translate.google.com" title="ترجمة جوجل" target="_blank" rel="nofollow"> ترجمة - جوجل</a></li>
										<li><a href="http://www.palestineremembered.com" title="موقع فلسطين في الذاكرة يحوي معلومات عن جميع المدن والقرى الفلسطينية" target="_blank" rel="nofollow">فلسطين في الذاكرة</a></li>
										<li><a href="http://www.paldf.net/" title="اضخم منتدى فلسطيني" target="_blank">شبكة فلسطين للحوار</a></li>
										<li><a href="http://www.multka.net/" title="منتديات الملتقى التربوي" target="_blank" rel="nofollow">الملتقى التربوي</a></li>
										<li><a href="http://www.pal-stu.com/" title="منتديات ملتقى طلاب فلسطين" target="_blank">ملتقى طلاب فلسطين</a></li>
										<li><a href="http://inshad.com/" title="النشيد بلون جديد ، شبكة إنشادكم العالمية المصدر الأول للنشيد و الفن الهادف" target="_blank" rel="nofollow">شبكة إنشادكم العالمية </a></li>
										<li><a href="http://p-weather.ps/" title="طقس فلسطين" target="_blank" rel="nofollow">طقس الوطن - فلسطين</a> </li>
										<li><a href="http://palweather.ps" title="طقس فلسطين" target="_blank" rel="nofollow">طقس فلسطين</a> </li>
										<li><a href="http://www.kooora.com/" title="اضخم موقع رياضي عربي" target="_blank" rel="nofollow">كوووورة</a></li>
										<li><a href="http://www.hihi2.com/" title="موقع رياضي متخصص في اخبار كرة القدم العربية والعالمية
										" target="_blank" rel="nofollow"> هاي كورة</a></li>
										<li><a href="http://pallap.com/m/2010/03/speed-net-test" title="سرعة اتصالك بالإنترنت الحقيقية" target="_blank">قياس سرعة الانترنت</a></li>
									</ul>
								</div>
								<div class="panel-footer"></div>
							</div>
						</div>	
						
						
						
						
						<div class="col-xs-6 col-sm-3">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h3 class="panel-title"> مواقع تـهـمـــك </h3>
								</div>
								<div class="panel-body">
									<ul>
										<li><a href="http://www.mp3quran.net/" title="المكتبة الصوتية للقرآن الكريم mp3 تضم عدد كبير من القراء و بعدة روايات و بعدة لغات , مع روابط تحميل مباشرة " target="_blank">القرآن الكريم mp3</a></li>
										<li><a href="http://www.islamway.com" title="موقع طريق الاسلام" target="_blank">طريق الإسلام</a></li>
										<li><a href="http://www.saaid.net/" title="موقع صيد الفوائد" target="_blank">صيد الفوائد</a></li>
										<li><a href="http://www.wathakker.info" title="موقع وذكّر" target="_blank">موقع وذكـر</a></li>
										<li><a href="http://www.islamstory.com" title="موقع قصة الاسلام بإشراف الدكتور راغب السرجاني" target="_blank">قـصـة الإسـلام</a></li>
										<li><a href="http://www.islamicfinder.org/index.php?lang=arabic" title="IslamicFinder" target="_blank">الباحث الاسلامي</a></li>
										<li><a href="http://www.islamweb.net/" title="موقع الشبكة الإسلامية" target="_blank">إسلام ويب</a></li>
										<li><a href="http://www.emanway.com/" title="موقع طريق الايمان" target="_blank" rel="nofollow">طريق الايمان</a></li>
										<li><a href="http://www.dorar.net/" title="لفحص صحة أي حديث " target="_blank" >الدرر السنية</a></li>
										<li><a href="http://www.quran-m.com" title="موسوعة الاعجاز العلمي في القرآن والسنة" target="_blank">موسوعة الاعجاز العلمي</a></li>
										<li><a href="http://akhawat.islamway.com/forum" title="اضخم منتدى للأخوات إسلامي" target="_blank">اخوات طريق الإسلام</a></li>								
									</ul>
								</div>
								<div class="panel-footer"></div>
							</div>
						</div>				
						
						
						
						<div class="col-xs-6 col-sm-3">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">أخـبـار - News </h3>
								</div>
								<div class="panel-body">
									<ul>
										<li><a href="http://aljazeera.net/" title="موقع قناة الجزيرة" target="_blank" rel="nofollow">الجزيرة نت</a></li>
										<li><a href="http://www.paltimes.net" title="فلسطين الآن | بوابتك إلى الحقيقة" target="_blank">فلسطين الآن</a></li>
										<li><a href="http://maannews.net" title="وكالة معاً الاخبارية" target="_blank" rel="nofollow">وكالة معا</a></li>
										<li><a href="http://safa.ps" title="وكالة االصحافة الفلسطينية - صفا" target="_blank" rel="nofollow">وكالة صفا</a></li>
										<li><a href="http://paltoday.ps" title="فلسطين اليوم" target="_blank" rel="nofollow">وكالة فلسطين اليوم </a></li>
										<li><a href="http://felesteen.ps/" title="موقع صحيفة فلسطين" target="_blank"> فلسطين أون لاين</a></li>
										<li><a href="http://www.islammemo.cc" title="شركة مفكرة الإسلام" target="_blank" rel="nofollow">مفكرة الإسلام</a></li>
										<li><a href="http://www.islam-online.net" title="islam online" target="_blank" >إسلام أون لاين</a></li>
										<li><a href="http://www.almokhtsar.com" title="موقع المختصر للأخبار" target="_blank" rel="nofollow">المختصر </a></li>
										<li><a href="http://almoslim.net" title="موقع المسلم" target="_blank">موقع المـسلم</a></li>
										<li><a href="http://aitnews.com" title=" البوابة العربية للأخبار التقنية " target="_blank" rel="nofollow"> البوابة العربية للأخبار التقنية</a></li>
										
										
									</ul>
								</div>
								<div class="panel-footer"></div>
							</div>
						</div>
						
						
						<div class="col-xs-6 col-sm-2 "  style="font-family:KufiArabic;">
							
							<a class="btn btn-social btn-lg btn-google-plus btn-block" href="http://www.google.com/" title="جـوجــل"  target="_blank" rel="nofollow"><i class="fa fa-google"></i>&nbsp;  |  &nbsp;<b>جوجل</b></a>
							
							<a class="btn btn-social btn-lg btn-facebook btn-block" href="http://www.facebook.com/" title="حسابك في الفيسبوك" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i>&nbsp;  |  &nbsp;<b>فيسبوك</b></a>
							
							<a class="btn btn-social btn-lg btn-youtube btn-block" href="http://www.youtube.com/" title="يوتيوب " target="_blank" rel="nofollow"><i class="fa fa-youtube"></i>&nbsp;  |  &nbsp;<b>يوتيوب</b></a>
							
							<a class="btn btn-social btn-lg btn-twitter btn-block" href="http://www.twitter.com/" title="تويتر " target="_blank" rel="nofollow"><i class="fa fa-twitter"></i>&nbsp;  |  &nbsp;<b>تويتر</b></a>
							
							<a class="btn btn-social btn-lg btn-linkedin btn-block" href="http://www.linkedin.com/" title="لنكد إن " target="_blank" rel="nofollow"><i class="fa fa-linkedin"></i>&nbsp;  |  &nbsp;<b>لنكد إن</b></a>
							
							<a class="btn btn-social btn-lg btn-github btn-block" href="http://ar.wikipedia.org" title="wikipedia موسوعة ويكيبيديا" target="_blank" rel="nofollow"><i>W</i>&nbsp;  |  &nbsp;<b>ويكيبيديا</b></a>
							
							<a class="btn btn-social btn-lg btn-linkedin btn-block" href="http://www.outlook.com/" title="الدخول الى  ايميل" target="_blank" rel="nofollow"><i class="fa fa-windows"></i>&nbsp;  |  &nbsp;<b>Outlook</b></a>
							
							<a class="btn btn-social btn-lg btn-flickr btn-block" href="http://www.flickr.com/" title="فليكر " target="_blank" rel="nofollow"><i class="fa fa-flickr"></i>&nbsp;  |  &nbsp;<b>فليكر</b></a>
							
                            <a class="btn btn-social btn-lg btn-weibo btn-block" href="http://www.soundcloud.com/" title="soundcloud " target="_blank" rel="nofollow"><i class="fa fa-soundcloud"></i>&nbsp;  |  &nbsp;<b>SoundCloud</b></a>
							
							
							
						</div>







						
					</div>	
				</div>
				
				<?PHP 
					$footer='on'; 
				include '../include/main_footer.php'; ?>
				</div></td>
		</tr>
	</table>
</body>
</html>