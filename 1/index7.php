<!DOCTYPE html>
<html lang="ar">
	<head>
		<title>شبكة بال لاب </title>
		<META NAME="description" CONTENT="دليل فلسطين و بث مباشر للقنوات والإذاعات فيديو صوتيات أفلام مسلسلات راديو تلفزيون مشاهدة وكل ما تريده من فلسطين القدس غزة رام الله">
		
		<?php include_once("include/all_header.php") ?>
		
		
		<style type="text/css">
			@font-face {
			font-family: KufiArabic;
			src: url(fonts/KufiArabic-Regular.ttf);
			}
			a {
			color: #666 ;
			}
			.panell li a {
			line-height: 37px;
			}
			.panell li a:hover {
			font-size: 15px;
			background-color: rgb(252, 248, 227);
			text-decoration: none;
			line-height: 37px;
			}
			.logo {
			background-color: #50147A;
			border-bottom-left-radius: 33px;
			border-bottom-right-radius: 33px;
			width: 100%;
			}
			.txt_top_visit {
			color: gray;
			font-size: 11px;
			}
			
			.logo_top_visit {
			color: #f4511e;
			font: normal normal normal 40px/1 FontAwesome;
			}
			.FAwesome_size{
			font-size: 2.7em;
			}
			.graph {
			font-size: 15px;
			background: #F9F1FF;
			height:auto;
			width:90%;
			-moz-border-radius: 25px;
			border-radius: 24px ;
			display: inline-block;
			margin-left:auto;
			margin-right:auto;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}
			
			.graph:hover {
			background-color:#FFF;
			-moz-border-radius:0px;
			border-radius: 0px;
			}
			.menu {
			position: relative;
			opacity:0.5;
			}
			.menu:hover {
			opacity:1;
			}
			.menu ul {
			list-style: none;
			margin: 0;
			padding: 0;
			}
			
			.menu ul li {
			display: block;
			float: left;
			list-style: none;
			margin: 0;
			padding: 0;
			position: relative;
			}
			.menu ul li a {
			display: block;
			padding: 3px 8px;
			text-decoration: none;
			}
			.menu ul li a:hover {
			background: #50147A;
			opacity:1.0;
			}
			.menu ul li a.active, .menu ul li a.active:hover {
			background: #000;
			}
			
			.pray{
			position: fixed;
			float: left;
			margin-left: -5px;
			bottom: 200px;
			left: 0;
			background: white;
			-webkit-animation:bounceIn 2s;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}
			.pray:hover{
			margin-left: 0px;
			background: #F9F1FF;
			color: #50147A;
			}
		</style>
		
		
	</head>
	
	<body dir="rtl" bgcolor="#333333">
		
		<?php include_once("include/all_body.php") ?>
		
		<table   width="95%" align="center"  >
			<tr>
				<td>
					<div   class="header1" dir="rtl" >
					
						<div class="row" dir="rtl">
							<div class="col-xs-12 col-sm-4">
								
								<div class="menu" align="left">
									<ul>
										<li><a target="_blank" rel="nofollow" href="https://www.facebook.com/pallap2"><img alt="تابعنا على الفيسبوك" src="../up/s/fb.png" /></a></li>
										<li><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/pallearn"><img alt="قناتنا على اليوتيوب" src="../up/s/y.png" /></a></li>
										<li><a target="_blank" rel="nofollow" href="https://twitter.com/pallap1"><img alt="تابعنا على تويتر" src="../up/s/t.png" /></a></li>
									</ul>
								</div>
								
							</div>
							<div class="col-xs-12 col-sm-4"  >
								<div class="logo">
									<center>
										<a href="/" title="الى الرئيسية"> <img id="logo" src="up/logo-color.png" alt="pallap بال لاب" /></a>
									</center>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4">
								<div class="text-right text-muted">
									<h3> صفحة إبدأ  </h3>
								</div>
							</div>
						</div>
						
						
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="container">
						<div class="row" dir="rtl" style="font-family:Tahoma;">
							<div class="col-xs-12 col-sm-3">
								<h3 class="panel-title"> مواقع تـهـمـــك </h3>
								<div class="panell">
									<div class="graph">
										<ul>
											<li><a href="https://coronavirus.app/" title="احصائيات عالمية مباشرة لفايروس كوفيد19" target="_blank"  rel="nofollow">  خارطة فايروس كورونا </a></li>
											<li><a href="https://webteb.com" title="وقع طبي وصحة شامل" target="_blank"  rel="nofollow"> ويب طب </a></li>
											<li><a href="https://mawdoo3.com" title="موسوعة,عربية,شاملة,موضوع"  rel="nofollow" target="_blank"> موضوع.كوم  </a></li>
											<li><a href="https://www.aliqtisadi.ps" title="الأولى للأعمال وريادة الشباب في فلسطين" target="_blank">الاقتصادي </a></li>
											<li><a href="https://arageek.com" title="منصـة إعلاميـة تثقيفيــة رقميــة شامــلة" target="_blank" rel="nofollow">اراجيك</a> </li>
											<li><a href="https://akhawat.islamway.net" title="اضخم منتدى للأخوات إسلامي" target="_blank">اخوات طريق الإسلام</a></li>
											<li><a href="https://aitnews.com" title=" البوابة العربية للأخبار التقنية " target="_blank" rel="nofollow"> البوابة للأخبار التقنية</a></li>
											<li><a href="https://pallap.com/m/2010/03/speed-net-test/" title="سرعة اتصالك بالإنترنت الحقيقية" target="_blank"> سرعة الانترنت</a></li>
											<li><i class="fa fa-keyboard-o" aria-hidden="true"></i>  <a href="https://www.keybr.com/practice" title="تعلم الكتابة دون النظر للموحة المفاتيح  keybr" target="_blank" rel="nofollow"> keybr</a> /
											<a href="https://www.ratatype.com/typing-test/test/" title="ratatype" target="_blank" rel="nofollow">RataType </a> </li>
											<li><a href="https://www.grammarly.com" title="تصحيح كتابتك بالانجليزية" target="_blank" rel="nofollow"> Grammarly</a></li>
											<li><a href="https://droosonline.com/" title=" دروس اونلاين" target="_blank" rel="nofollow">دروس اونلاين</a></li>
											
											
										</ul>
									</div>
								</div>
								<br>
								<div class="panell">
									<div class="graph">
										<ul>
											<li><a href="https://www.kooora.com"  title="اضخم موقع رياضي عربي" target="_blank" rel="nofollow">كوووورة</a>
												• <a href="https://www.yallakora.com" title=" موقع رياضي" target="_blank" rel="nofollow">يلا كورة</a>
												• <a href="https://www.hihi2.com" title="موقع رياضي متخصص في اخبار كرة القدم العربية والعالمية" target="_blank" rel="nofollow"> هاي كورة</a>
												
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<h3 class="panel-title"> إسلاميات </h3>
									<div class="panell">
										<div class="graph">
											<ul>
												<li> الكريم > <a href="https://www.mp3quran.net/"   title="المكتبة الصوتية للقرآن الكريم mp3 تضم عدد كبير من القراء و بعدة روايات و بعدة لغات , مع روابط تحميل مباشرة " target="_blank">صوت </a> • <a href="https://pallap.com/quranflash/" title="تصفح القران الكريم - فلاش" target="_blank" rel="nofollow">قراءة</a></li>
												<li><a href="https://islamway.net"   title="موقع طريق الاسلام" target="_blank">طريق الإسلام</a></li>
												<li><a href="https://www.saaid.net"   title="موقع صيد الفوائد" target="_blank">صيد الفوائد</a></li>
												<li><a href="https://islamqa.info/ar" title="موقع الإسلام سؤال وجواب موقع دعوي، علمي تربوي يهدف إلى تقديم الفتاوى والإجابات العلمية المؤصلة"  rel="nofollow" target="_blank" >الإسلام س ج</a></li>
												<li><a href="https://scholar.google.com/schhp?hl=ar"  rel="nofollow" title="الباحث العلمي - أكبر قاعدة بيانات للكتب والمخطوطات والأبحاث " target="_blank" >الباحث العلمي</a></li>
												<li><a href="https://www.islamicfinder.org"   title="IslamicFinder" target="_blank">الباحث الاسلامي</a></li>
												<li><a href="https://www.islamweb.net"    title="موقع الشبكة الإسلامية" target="_blank">إسلام ويب</a></li>
												<li><a href="https://www.dorar.net"    title="لفحص صحة أي حديث " target="_blank" >الدرر السنية</a></li>
												<li><a href="https://www.nabulsi.com/"    title="موسوعة النابلسي للعلوم الإسلامية" target="_blank">موسوعة النابلسي</a></li>
												<li><a href="https://almoslim.net/"   title="موقع المسلم" target="_blank">موقع المـسلم</a></li>
												<li><a href="https://www.islamstory.com"   title="موقع قصة الاسلام بإشراف الدكتور راغب السرجاني" target="_blank">قـصـة الإسـلام</a></li>
												<li><a href="https://www.quran-m.com"    title="موسوعة الاعجاز العلمي في القرآن والسنة" target="_blank">موسوعة الاعجاز العلمي</a></li>
												<li><a href="https://www.albetaqa.site"  title=" موقع البطاقة الدعوي" target="_blank">البطاقة</a></li>
												<li><a href="https://islamhouse.com"  title="islamhouse" target="_blank">دار الإسلام</a></li>
												
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<h3 class="panel-title"> فلسطينيات </h3>
									<div class="panell">
										<div class="graph">
											<ul>
												<li><a href="https://safa.ps" title="وكالة االصحافة الفلسطينية - صفا" target="_blank" rel="nofollow">وكالة صفا</a></li>
												<li><a href="http://www.pmd.ps/"  target="_blank" rel="nofollow">الأرصاد الجوية </a> / <a href="https://pallap.com/rainradar"  target="_blank" rel="nofollow">رادار المطر </a> </li>
												<li><a href="https://www.palinfo.com/" title=" المركز الفلسطيني للإعلام" target="_blank"> المركز الفلسطيني للإعلام</a></li>
												<li><a href="https://samanews.ps"  target="_blank" rel="nofollow">سما الاخباية</a></li>
												<li><a href="https://maannews.net" title="وكالة معاً الاخبارية" target="_blank" rel="nofollow">وكالة معا</a></li>
												<li><a href="https://almajd.ps"  target="_blank"  >المجد الأمني</a></li>
												<li><a href="https://akka.ps/"  target="_blank" title="موقع يهتم بالشأن الإسرائيلي"  rel="nofollow">عـكـا  -الشؤون الإسرائيلية</a></li>
												<li><a href="https://paltoday.ps" title="فلسطين اليوم" target="_blank" rel="nofollow">وكالة فلسطين اليوم </a></li>
												<li><a href="https://palestine.paldf.net/"  target="_blank">فلسطين . نت  </a></li>
												<li><a href="https://paltimes.net/"  target="_blank">فلسطين الآن</a></li>
												<li><a href="https://shobiddak.com" title=" شو بدك من فلسطين" target="_blank" rel="nofollow">شو بدك من فلسطين</a></li>
												<li><a href="https://bit.ly/alwatanvoice_com" title="دنيا الوطن صحيفة" target="_blank" rel="nofollow">دنيا الوطن</a></li>
												<li><a href="https://www.felesteen.ps" title="موقع صحيفة فلسطين" target="_blank"> فلسطين أون لاين</a></li>
												<li><a href="https://ishraqat.info/" title="مجلة اشراقات" target="_blank">اشراقات</a></li>
												
												
												
												
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 btn-group-vertical text-right "  style="font-family:KufiArabic;">
									
									<div class="row">
										<a class="btn btn-link col-sm-4" href="https://www.youtube.com" title="يوتيوب " target="_blank" rel="nofollow">
										<i class="fab fa-youtube FAwesome_size" style="color:red;"></i><div class="txt_top_visit">يوتيوب </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://fb.com" title="حسابك في الفيسبوك" target="_blank" rel="nofollow">
										<i class="fab fa-facebook-square FAwesome_size" style="color:#4267b2;"></i><div class="txt_top_visit">فيسبوك </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://www.google.com" title="جـوجــل"  target="_blank" rel="nofollow">
										<i class="fab fa-google FAwesome_size"></i><div class="txt_top_visit">جــوجــل </div></a>
										
										
									</div>
									
									<div class="row">
										<a class="btn btn-link col-sm-4" href="https://twitter.com" title="تويتر " target="_blank" rel="nofollow">
										<i class="fab fa-twitter FAwesome_size" style="color:#1da1f2;"></i><div class="txt_top_visit">تويتر </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://ar.wikipedia.org" title="wikipedia موسوعة ويكيبيديا" target="_blank" rel="nofollow">
										<i class="fab fa-wikipedia-w FAwesome_size" style="color:#000000;"></i><div class="txt_top_visit">ويكيبيديا </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://soundcloud.com" title="ساوند كلاود"  target="_blank" rel="nofollow">
										<i class="fab fa-soundcloud FAwesome_size" style="color:#f50;"></i><div class="txt_top_visit">SoundCloud </div></a>
										
									</div>
									
									
									<div class="row">
										<a class="btn btn-link col-sm-4" href="https://outlook.com" title="الدخول الى  ايميل"  target="_blank" rel="nofollow">
										<i class="fab fa-windows FAwesome_size"  style="color:#0067b8;"></i><div class="txt_top_visit">Outlook </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://www.instagram.com/" title="انستغرام "   target="_blank" rel="nofollow">
										<i class="fab fa-instagram FAwesome_size"  style="color:#ED4A55;"></i><div class="txt_top_visit">انستغرام </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://bing.com" title="محرك بحث بنج " target="_blank" rel="nofollow">
										<i class="fab fa-microsoft FAwesome_size"  style="color:#ED4A55;"></i><div class="txt_top_visit">Ms Bing </div></a>
										
									</div>
									
									
									<div class="row">
										<a class="btn btn-link col-sm-4" href="https://yahoo.com" title="ياهو "   target="_blank" rel="nofollow">
										<i class="fab fa-yahoo FAwesome_size"  style="color:#2d1152;"></i><div class="txt_top_visit">ياهو </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://web.whatsapp.com" title="واتساب  WhatsApp "   target="_blank" rel="nofollow">
										<i class="fab fa-whatsapp FAwesome_size"  style="color:#43d854;"></i><div class="txt_top_visit">واتساب </div></a>
										
										<a class="btn btn-link col-sm-4" href="https://linkedin.com" title="www.linkedin.com" target="_blank" rel="nofollow">
										<i class="fab fa-linkedin FAwesome_size"  style="color:#0084bf;"></i><div class="txt_top_visit">LinkedIn </div></a>
									</div>
									
									<div class="row">
										<a class="btn btn-link col-sm-4"  href="https://www.aa.com.tr/ar" title="وكالة الأناضول"  target="_blank" rel="nofollow">
										<i class="logo_top_visit"><img style="background-color: #00447C;"; height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Anadolu_Agency_Logo.jpg/50px-Anadolu_Agency_Logo.jpg"></i><div class="txt_top_visit">وكالة الأناضول</div></a>
										
										<a class="btn btn-link col-sm-4"  href="https://www.aljazeera.net" title="موقع قناة الجزيرة"  target="_blank" rel="nofollow">
										<i class="logo_top_visit"><img alt="قناة الجزيرة  " src="https://upload.wikimedia.org/wikipedia/en/thumb/7/71/Aljazeera.svg/40px-Aljazeera.svg.png"></i><div class="txt_top_visit">الجزيرة نت  </div></a>
										
										<a class="btn btn-link col-sm-4"  href="https://translate.google.com/" title="ترجمة - جوجل"  target="_blank" rel="nofollow">
											<i class="logo_top_visit"><img src="https://pallap.com/up/1517571216.jpg"></i>
										<div class="txt_top_visit">ترجمة جوجل  </div></a>
										
									</div>
									
									<div class="row">
										<a class="btn btn-link col-sm-4"  href="https://duckduckgo.com/" title=" محرك بحث على الإنترنت يركز على حماية خصوصية الباحثين"  target="_blank" rel="nofollow">
										<i class="logo_top_visit">
												<img src="https://upload.wikimedia.org/wikipedia/en/8/88/DuckDuckGo_logo.svg" width="75">
											</i><div class="txt_top_visit">دك دك غو</div></a>
										
										<a class="btn btn-link col-sm-4" href="https://web.telegram.org/z/" title="web.telegram" target="_blank" rel="nofollow">
										<i class="fab fa-telegram FAwesome_size"  style="color:rgb(74 149 214);"></i><div class="txt_top_visit">تلغرام </div></a>											
									</div>
									
									
									<div class="row">
										<a  class="btn" href="https://www.facebook.com/pallap2" target="_blank"  rel="nofollow" title="إقترح إضافة موقع للصفحة"> شاركنا برأيك ♥  أو اقترح اضافة موفع  </a>
										
									</div>
									
									
									
									
								</div>
								<?PHP
									$footer='on';
									include_once("include/all_footer.php");
									
									
									
								?>
							</div>
						</td>
					</tr>
				</table>
				
				
				<div class="pray">
					<a class=" btn btn-link" href="https://www.prayer-times.info/palestine/alquds/" title="أوقات الصلاة "   target="_blank" rel="nofollow">
						أوقات الصلاة    <br>
						بوقيت القدس
					</a>
				</div>
				
			</body>
		</html>
		