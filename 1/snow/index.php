<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="../style/theme.min.css" type="text/css" />
		<title>صفحة إبدأ  | PallaP | دليل فلسطين</title>
		<META NAME="keywords" CONTENT=" دليل فلسطين الالكتروني ، إلكتروني ، شامل ، صفحة ، البداية ، ابدا ، إبدأ ، إبدأ ، صفحه ابدأ ، أبدأ، صفحة إبدأ ، مواقع ، رائعة، دليل ، صفحه ابدأ ، صفحة ابدأ ، بال لاب، خدمة، خدمات ، مدونة، فلسطين ، دليل فلسطين ">
		<META NAME="description" CONTENT="   دليل فلسطين الالكتروني الشامل وكل ما تريده من مواقع تهمك في صفحة واحدة إبدأ  ابدأ ابدا أبدأ الفلسطينية افضل المواقع  إحدى خدمات شبكة بال لاب مخصصة لتكون افضل صفحة بداية ">

		<style type="text/css">
			@font-face {
			font-family: KufiArabic;
			src: url(/fonts/KufiArabic-Regular.ttf);
			}
			a {
            color: #666 ;
			}
			.panell li a {
			line-height: 37px;
			}
			.panell li a:hover {
			font-size: 20px;
			background-color: rgb(252, 248, 227);
			text-decoration: none;
			line-height: 37px;
			}
			.logo {
			background-color: #50147A;
			border-bottom-left-radius: 33px;
			border-bottom-right-radius: 33px;
			width: 100%;
			}
			.graph {
			font-size: 16px;
			background: #F9F1FF;
			height:auto;
			width:90%;
			-moz-border-radius: 25px;
			border-radius: 25px ;
			display: inline-block;
			margin-left:auto;
			margin-right:auto;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}

			.graph:hover {
			background-color:#FFF;
			width:95%;
			height:auto;
			-moz-border-radius:0px;
			border-radius: 0px;
			font-size:17px;
			}
			.menu {
			position: relative;
			opacity:0.5;
			}
			.menu:hover {
			opacity:1;
			}
			.menu ul {
			list-style: none;
			margin: 0;
			padding: 0;
			}

			.menu ul li {
			display: block;
			float: left;
			list-style: none;
			margin: 0;
			padding: 0;
			position: relative;
			}
			.menu ul li a {
			display: block;
			padding: 3px 8px;
			text-decoration: none;
			}
			.menu ul li a:hover {
			background: #50147A;
			opacity:1.0;
			}
			.menu ul li a.active, .menu ul li a.active:hover {
			background: #000;
			}

		</style>
        <?php include_once("../include/all_header.php") ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
<script src="https://raw.githubusercontent.com/kopipejst/jqSnow/master/jquery.snow.js"></script>
	</head>

	<body dir="rtl" bgcolor="#333333">
	<script>
$(document).ready( function(){
    $.fn.snow();
});
</script>
        <?php include_once("../include/all_body.php") ?>

        <table   width="95%" align="center"  >
			<tr>
				<td><div   class="header1" dir="rtl" >
					<div class="row" dir="rtl">
						<div class="col-xs-12 col-sm-4">

							<div class="menu" align="left">
								<ul>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_facebook"><img alt="تابعنا على الفيسبوك" src="../up/s/fb.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_youtube"><img alt="قناتنا على اليوتيوب" src="../up/s/y.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_twitter"><img alt="تابعنا على تويتر" src="../up/s/t.png" /></a></li>
								</ul>
							</div>

						</div>
						<div class="col-xs-12 col-sm-4"  >
							<div class="logo">
								<center>
									<a href="/" title="الى الرئيسية"> <img id="logo" src="../up/logo-color.png" alt="pallap بال لاب" /></a>
								</center>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="text-right text-muted">
								<h3> صفحة إبدأ  - <a  class="btn" href="http://pallap.com/m/contact-us/" target="_blank" title="إقترح إضافة موقع للصفحة">إقترح موقع</a> </h3>
							</div>
						</div>
					</div>
				</div></td>
			</tr>
			<tr>
				<td><div id="container">
					<div class="row" dir="rtl" style="font-family:Tahoma;">
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title">مـنـتـديات ومنوعات</h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/G_Translate_" title="ترجمة جوجل" target="_blank" rel="nofollow"> ترجمة - جوجل</a></li>
										<li><a href="http://bit.ly/Pal_Remembered" title="موقع فلسطين في الذاكرة يحوي معلومات عن جميع المدن والقرى الفلسطينية" target="_blank" rel="nofollow">فلسطين في الذاكرة</a></li>
										<li><a href="http://bit.ly/paldf_net" title="اضخم منتدى فلسطيني" target="_blank">شبكة فلسطين للحوار</a></li>
										<li><a href="http://bit.ly/1UiEZeO" title="منتديات الملتقى التربوي" target="_blank" rel="nofollow">الملتقى التربوي</a></li>
										<li><a href="http://bit.ly/pal_stu" title="منتديات ملتقى طلاب فلسطين" target="_blank">ملتقى طلاب فلسطين</a></li>
										<li><a href="http://bit.ly/1RKLGHa" title="النشيد بلون جديد ، شبكة إنشادكم العالمية المصدر الأول للنشيد و الفن الهادف" target="_blank" rel="nofollow">شبكة إنشادكم العالمية </a></li>
										<li><a href="http://bit.ly/p_weather" title="طقس فلسطين" target="_blank" rel="nofollow">طقس الوطن - فلسطين</a> </li>
										<li><a href="http://bit.ly/palweather_ps" title="طقس فلسطين" target="_blank" rel="nofollow">طقس فلسطين</a> </li>
										<li><a href="http://bit.ly/koooooora" title="اضخم موقع رياضي عربي" target="_blank" rel="nofollow">كوووورة</a></li>
										<li><a href="http://bit.ly/1lEw1gu" title="موقع رياضي متخصص في اخبار كرة القدم العربية والعالمية
										" target="_blank" rel="nofollow"> هاي كورة</a></li>
										<li><a href="http://pallap.com/m/2010/03/speed-net-test" title="سرعة اتصالك بالإنترنت الحقيقية" target="_blank">قياس سرعة الانترنت</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title"> مواقع تـهـمـــك </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/mp3Quran" title="المكتبة الصوتية للقرآن الكريم mp3 تضم عدد كبير من القراء و بعدة روايات و بعدة لغات , مع روابط تحميل مباشرة " target="_blank">القرآن الكريم mp3</a></li>
										<li><a href="http://bit.ly/islamway" title="موقع طريق الاسلام" target="_blank">طريق الإسلام</a></li>
										<li><a href="http://bit.ly/saaid_net" title="موقع صيد الفوائد" target="_blank">صيد الفوائد</a></li>
										<li><a href="http://bit.ly/wathakker" title="موقع وذكّر" target="_blank">موقع وذكـر</a></li>
										<li><a href="http://bit.ly/islamqa_info" title="موقع الإسلام سؤال وجواب موقع دعوي، علمي تربوي يهدف إلى تقديم الفتاوى والإجابات العلمية المؤصلة" target="_blank" >الإسلام س ج</a></li>
										<li><a href="http://bit.ly/islamstory_com" title="موقع قصة الاسلام بإشراف الدكتور راغب السرجاني" target="_blank">قـصـة الإسـلام</a></li>
										<li><a href="http://bit.ly/islamicfinder" title="IslamicFinder" target="_blank">الباحث الاسلامي</a></li>
										<li><a href="http://bit.ly/islamweb_net" title="موقع الشبكة الإسلامية" target="_blank">إسلام ويب</a></li>
										<li><a href="http://bit.ly/emanway" title="موقع طريق الايمان" target="_blank" rel="nofollow">طريق الايمان</a></li>
										<li><a href="http://bit.ly/dorar_net" title="لفحص صحة أي حديث " target="_blank" >الدرر السنية</a></li>
										<li><a href="http://bit.ly/quran-m_com" title="موسوعة الاعجاز العلمي في القرآن والسنة" target="_blank">موسوعة الاعجاز العلمي</a></li>
										<li><a href="http://bit.ly/akhawat_islamway" title="اضخم منتدى للأخوات إسلامي" target="_blank">اخوات طريق الإسلام</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title">أخـبـار - News </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/aljazeera_net" title="موقع قناة الجزيرة" target="_blank" rel="nofollow">الجزيرة نت</a></li>
										<li><a href="http://bit.ly/safa_ps" title="وكالة االصحافة الفلسطينية - صفا" target="_blank" rel="nofollow">وكالة صفا</a></li>
										<li><a href="http://bit.ly/alwatanvoice_com" title="دنيا الوطن صحيفة" target="_blank" rel="nofollow">دنيا الوطن</a></li>
										<li><a href="http://bit.ly/palinfo_com" title=" المركز الفلسطيني للإعلام" target="_blank"> المركز الفلسطيني للإعلام</a></li>
										<li><a href="http://bit.ly/paltimes_net" title="فلسطين الآن | بوابتك إلى الحقيقة" target="_blank">فلسطين الآن</a></li>
										<li><a href="http://bit.ly/maannews_net" title="وكالة معاً الاخبارية" target="_blank" rel="nofollow">وكالة معا</a></li>
										<li><a href="http://bit.ly/felesteen_ps" title="موقع صحيفة فلسطين" target="_blank"> فلسطين أون لاين</a></li>
										<li><a href="http://bit.ly/paltoday_ps" title="فلسطين اليوم" target="_blank" rel="nofollow">وكالة فلسطين اليوم </a></li>
										<li><a href="http://bit.ly/qudsn_ps" title="موقع شبكة قدس الاخبارية" target="_blank" rel="nofollow">شبكة قدس</a></li>
										<li><a href="http://bit.ly/almokhtsar_com" title="موقع المختصر للأخبار" target="_blank" rel="nofollow">المختصر </a></li>
                                        <li><a href="http://bit.ly/islammemo_cc" title="شركة مفكرة الإسلام" target="_blank" rel="nofollow">مفكرة الإسلام</a></li>
										<li><a href="http://bit.ly/almoslim_net" title="موقع المسلم" target="_blank">موقع المـسلم</a></li>
										<li><a href="http://bit.ly/aitnews_com" title=" البوابة العربية للأخبار التقنية " target="_blank" rel="nofollow"> البوابة للأخبار التقنية</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 btn-group-vertical text-right "  style="font-family:KufiArabic;">
							<a class="btn btn-link btn-lg " href="http://bit.ly/google_com1" title="جـوجــل"  target="_blank" rel="nofollow"><i class="fa fa-google"></i>&nbsp;  |  &nbsp;<b>جــوجــل</b></a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/facebook_com1" title="حسابك في الفيسبوك" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i>&nbsp;  |  &nbsp;<b>فيسبوك</b></a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/youtube_com1" title="يوتيوب " target="_blank" rel="nofollow"><i class="fa fa-youtube"></i>&nbsp;  |  &nbsp;<b>يوتيوب</b></a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/twitter_com1" title="تويتر " target="_blank" rel="nofollow"><i class="fa fa-twitter"></i>&nbsp;  |  &nbsp;تويتر</a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/ar_wikipedia_org" title="wikipedia موسوعة ويكيبيديا" target="_blank" rel="nofollow">W&nbsp;  |  &nbsp;ويكيبيديا</a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/soundcloud_com1" title="soundcloud" target="_blank" rel="nofollow"><i class="fa fa-soundcloud"></i>&nbsp;  |  &nbsp;SoundCloud</a>
							<a class="btn btn-link btn-lg"  href="http://bit.ly/outlook_com1" title="الدخول الى  ايميل" target="_blank" rel="nofollow"><i class="fa fa-windows"></i>&nbsp;  |  &nbsp;Outlook</a>
							<a class="btn btn-link"         href="http://bit.ly/linkedin_com1" title="www.linkedin.com" target="_blank" rel="nofollow"><i class="fa fa-linkedin"></i>&nbsp;  |  &nbsp;لنكد إن</a>
							<a class="btn btn-link"         href="http://bit.ly/flickr_com1" title="فليكر " target="_blank" rel="nofollow"><i class="fa fa-flickr"></i>&nbsp;  |  &nbsp;فليكر</a>
						    <a class="btn btn-link"         href="http://bit.ly/About_com" title="اباوت " target="_blank" rel="nofollow"><b>About.com</b></a>
						    <a class="btn btn-link"         href="http://bit.ly/Trello_com" title=" trello" target="_blank" rel="nofollow"><b>Trello</b></a> </div>
					</div>
				</div>
				<?PHP
					$footer='on';
				include_once("../include/all_footer.php");

             

                 ?>
				</div></td>
		</tr>
	</table>
</body>
</html>