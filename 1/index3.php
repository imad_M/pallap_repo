<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="../style/theme.min.css" type="text/css" />
		<title>���� ����  | PallaP | ���� ������</title>
		<META NAME="keywords" CONTENT=" ���� ������ ���������� � �������� � ���� � ���� � ������� � ���� � ���� � ���� � ���� ���� � ���á ���� ���� � ����� � ����ɡ ���� � ���� ���� � ���� ���� � ��� ��ȡ ���ɡ ����� � ����ɡ ������ � ���� ������ ">
		<META NAME="description" CONTENT="   ���� ������ ���������� ������ ��� �� ����� �� ����� ���� �� ���� ����� ����  ���� ���� ���� ���������� ���� �������  ���� ����� ���� ��� ��� ����� ����� ���� ���� ����� ">

		<style type="text/css">
			@font-face {
			font-family: KufiArabic;
			src: url(/fonts/KufiArabic-Regular.ttf);
			}
			a {
            color: #666 ;
			}
			.panell li a {
			line-height: 37px;
			}
			.panell li a:hover {
			font-size: 20px;
			background-color: rgb(252, 248, 227);
			text-decoration: none;
			line-height: 37px;
			}
			.logo {
			background-color: #50147A;
			border-bottom-left-radius: 33px;
			border-bottom-right-radius: 33px;
			width: 100%;
			}
			.graph {
			font-size: 16px;
			background: #F9F1FF;
			height:auto;
			width:90%;
			-moz-border-radius: 25px;
			border-radius: 25px ;
			display: inline-block;
			margin-left:auto;
			margin-right:auto;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}

			.graph:hover {
			background-color:#FFF;
			width:95%;
			height:auto;
			-moz-border-radius:0px;
			border-radius: 0px;
			font-size:17px;
			}
			.menu {
			position: relative;
			opacity:0.5;
			}
			.menu:hover {
			opacity:1;
			}
			.menu ul {
			list-style: none;
			margin: 0;
			padding: 0;
			}

			.menu ul li {
			display: block;
			float: left;
			list-style: none;
			margin: 0;
			padding: 0;
			position: relative;
			}
			.menu ul li a {
			display: block;
			padding: 3px 8px;
			text-decoration: none;
			}
			.menu ul li a:hover {
			background: #50147A;
			opacity:1.0;
			}
			.menu ul li a.active, .menu ul li a.active:hover {
			background: #000;
			}

		</style>
        <?php include_once("../include/all_header.php") ?>

	</head>

	<body dir="rtl" bgcolor="#333333">
        <?php include_once("../include/all_body.php") ?>

        <table   width="95%" align="center"  >
			<tr>
				<td><div   class="header1" dir="rtl" >
					<div class="row" dir="rtl">
						<div class="col-xs-12 col-sm-4">

							<div class="menu" align="left">
								<ul>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_facebook"><img alt="������ ��� ��������" src="../up/s/fb.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_youtube"><img alt="������ ��� ��������" src="../up/s/y.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_twitter"><img alt="������ ��� �����" src="../up/s/t.png" /></a></li>
								</ul>
							</div>

						</div>
						<div class="col-xs-12 col-sm-4"  >
							<div class="logo">
								<center>
									<a href="/" title="��� ��������"> <img id="logo" src="../up/logo-color.png" alt="pallap ��� ���" /></a>
								</center>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="text-right text-muted">
								<h3> ���� ����  - <a  class="btn" href="http://pallap.com/m/contact-us/" target="_blank" title="����� ����� ���� ������">����� ����</a> </h3>
							</div>
						</div>
					</div>
				</div></td>
			</tr>
			<tr>
				<td><div id="container">
					<div class="row" dir="rtl" style="font-family:Tahoma;">
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title">���������� �������</h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="https://translate.google.com" title="����� ����" target="_blank" rel="nofollow"> ����� - ����</a></li>
										<li><a href="http://www.palestineremembered.com" title="���� ������ �� ������� ���� ������� �� ���� ����� ������ ����������" target="_blank" rel="nofollow">������ �� �������</a></li>
										<li><a href="http://www.paldf.net/" title="���� ����� �������" target="_blank">���� ������ ������</a></li>
										<li><a href="http://www.multka.net/" title="������� ������� �������" target="_blank" rel="nofollow">������� �������</a></li>
										<li><a href="http://www.pal-stu.com/" title="������� ����� ���� ������" target="_blank">����� ���� ������</a></li>
										<li><a href="http://inshad.com/" title="������ ���� ���� � ���� ������� �������� ������ ����� ������ � ���� ������" target="_blank" rel="nofollow">���� ������� �������� </a></li>
										<li><a href="http://p-weather.ps/" title="��� ������" target="_blank" rel="nofollow">��� ����� - ������</a> </li>
										<li><a href="http://palweather.ps" title="��� ������" target="_blank" rel="nofollow">��� ������</a> </li>
										<li><a href="http://www.kooora.com/" title="���� ���� ����� ����" target="_blank" rel="nofollow">�������</a></li>
										<li><a href="http://www.hihi2.com/" title="���� ����� ����� �� ����� ��� ����� ������� ���������
										" target="_blank" rel="nofollow"> ��� ����</a></li>
										<li><a href="http://pallap.com/m/2010/03/speed-net-test" title="���� ������ ��������� ��������" target="_blank">���� ���� ��������</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title"> ����� ��������� </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://www.mp3quran.net/" title="������� ������� ������ ������ mp3 ��� ��� ���� �� ������ � ���� ������ � ���� ���� , �� ����� ����� ������ " target="_blank">������ ������ mp3</a></li>
										<li><a href="http://www.islamway.com" title="���� ���� �������" target="_blank">���� �������</a></li>
										<li><a href="http://www.saaid.net/" title="���� ��� �������" target="_blank">��� �������</a></li>
										<li><a href="http://www.wathakker.info" title="���� �����" target="_blank">���� �����</a></li>
										<li><a href="http://islamqa.info/" title="���� ������� ���� ����� ���� ����� ���� ����� ���� ��� ����� ������� ��������� ������� �������" target="_blank" >������� � �</a></li>
										<li><a href="http://www.islamstory.com" title="���� ��� ������� ������ ������� ���� ��������" target="_blank">����� ��������</a></li>
										<li><a href="http://www.islamicfinder.org/index.php?lang=arabic" title="IslamicFinder" target="_blank">������ ��������</a></li>
										<li><a href="http://www.islamweb.net/" title="���� ������ ���������" target="_blank">����� ���</a></li>
										<li><a href="http://www.emanway.com/" title="���� ���� �������" target="_blank" rel="nofollow">���� �������</a></li>
										<li><a href="http://www.dorar.net/" title="���� ��� �� ���� " target="_blank" >����� ������</a></li>
										<li><a href="http://www.quran-m.com" title="������ ������� ������ �� ������ ������" target="_blank">������ ������� ������</a></li>
										<li><a href="http://akhawat.islamway.com/forum" title="���� ����� ������� ������" target="_blank">����� ���� �������</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title">������� - News </h3>
							<div class="panell">
								<div class="graph">

                                    <ul>
										<li><a href="http://aljazeera.net" title="���� ���� �������" target="_blank" rel="nofollow">������� ��</a></li>
										<li><a href="http://www.qudsn.ps" title="���� ���� ��� ���������" target="_blank" rel="nofollow">���� ���</a></li>
										<li><a href="http://www.alwatanvoice.com" title="���� ����� �����" target="_blank" rel="nofollow">���� �����</a></li>
										<li><a href="http://safa.ps" title="����� �������� ���������� - ���" target="_blank" rel="nofollow">����� ���</a></li>
										<li><a href="http://palinfo.com" title=" ������ ��������� �������" target="_blank"> ������ ��������� �������</a></li>
										<li><a href="http://www.paltimes.net" title="������ ���� | ������ ��� �������" target="_blank">������ ����</a></li>
										<li><a href="http://maannews.net" title="����� ���� ���������" target="_blank" rel="nofollow">����� ���</a></li>
										<li><a href="http://felesteen.ps/" title="���� ����� ������" target="_blank"> ������ ��� ����</a></li>
										<li><a href="http://paltoday.ps" title="������ �����" target="_blank" rel="nofollow">����� ������ ����� </a></li>
										<li><a href="http://www.almokhtsar.com" title="���� ������� �������" target="_blank" rel="nofollow">������� </a></li>
                                        <li><a href="http://www.islammemo.cc" title="���� ����� �������" target="_blank" rel="nofollow">����� �������</a></li>
										<li><a href="http://almoslim.net" title="���� ������" target="_blank">���� �������</a></li>
										<li><a href="http://aitnews.com" title=" ������� ������� ������� ������� " target="_blank" rel="nofollow"> ������� ������� �������</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 btn-group-vertical text-right "  style="font-family:KufiArabic;">
							<a class="btn btn-link btn-lg " href="http://www.google.com/" title="�������"  target="_blank" rel="nofollow"><i class="fa fa-google"></i>&nbsp;  |  &nbsp;<b>��������</b></a>
							<a class="btn btn-link btn-lg" href="http://www.facebook.com/" title="����� �� ��������" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i>&nbsp;  |  &nbsp;<b>������</b></a>
							<a class="btn btn-link btn-lg" href="http://www.youtube.com/" title="������ " target="_blank" rel="nofollow"><i class="fa fa-youtube"></i>&nbsp;  |  &nbsp;<b>������</b></a>
							<a class="btn btn-link btn-lg" href="http://www.twitter.com/" title="����� " target="_blank" rel="nofollow"><i class="fa fa-twitter"></i>&nbsp;  |  &nbsp;�����</a>
							<a class="btn btn-link btn-lg" href="http://ar.wikipedia.org" title="wikipedia ������ ���������" target="_blank" rel="nofollow">W&nbsp;  |  &nbsp;���������</a>
							<a class="btn btn-link btn-lg" href="http://soundcloud.com" title="soundcloud" target="_blank" rel="nofollow"><i class="fa fa-soundcloud"></i>&nbsp;  |  &nbsp;SoundCloud</a>
							<a class="btn btn-link btn-lg" href="http://outlook.com/" title="������ ���  �����" target="_blank" rel="nofollow"><i class="fa fa-windows"></i>&nbsp;  |  &nbsp;Outlook</a>
							<a class="btn btn-link" href="http://www.linkedin.com/" title="www.linkedin.com" target="_blank" rel="nofollow"><i class="fa fa-linkedin"></i>&nbsp;  |  &nbsp;���� ��</a>
							<a class="btn btn-link" href="http://www.flickr.com/" title="����� " target="_blank" rel="nofollow"><i class="fa fa-flickr"></i>&nbsp;  |  &nbsp;�����</a>
						<a class="btn btn-link" href="http://www.about.com/" title="����� " target="_blank" rel="nofollow"><b>About.com</b></a> </div>
					</div>
				</div>
				<?PHP
					$footer='on';
				include_once("../include/all_footer.php");



                 ?>
				</div></td>
		</tr>
	</table>
</body>
</html>