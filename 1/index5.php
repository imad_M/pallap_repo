<!DOCTYPE html>
<html>
	<head>
		<title>صفحة إبدأ  | PallaP | دليل فلسطين</title>
		<META NAME="keywords" CONTENT=" دليل فلسطين الالكتروني ، إلكتروني ، شامل ، صفحة ، البداية ، ابدا ، إبدأ ، إبدأ ، صفحه ابدأ ، أبدأ، صفحة إبدأ ، مواقع ، رائعة، دليل ، صفحه ابدأ ، صفحة ابدأ ، بال لاب، خدمة، خدمات ، مدونة، فلسطين ، دليل فلسطين ">
		<META NAME="description" CONTENT="   دليل فلسطين الالكتروني الشامل وكل ما تريده من مواقع تهمك في صفحة واحدة إبدأ  ابدأ ابدا أبدأ الفلسطينية افضل المواقع  إحدى خدمات شبكة بال لاب مخصصة لتكون افضل صفحة بداية ">
		
		<style type="text/css">
			@font-face {
			font-family: KufiArabic;
			src: url(/fonts/KufiArabic-Regular.ttf);
			}
			a {
            color: #666 ;
			}
			.panell li a {
			line-height: 37px;
			}
			.panell li a:hover {
			font-size: 20px;
			background-color: rgb(252, 248, 227);
			text-decoration: none;
			line-height: 37px;
			}
			.logo {
			background-color: #50147A;
			border-bottom-left-radius: 33px;
			border-bottom-right-radius: 33px;
			width: 100%;
			}
			.txt_top_visit {
				color: gray;
				font-size: 11px;
			}

			.logo_top_visit {
				color: #f4511e;
				font: normal normal normal 40px/1 FontAwesome;
				}
				.test a :hover {
						color:#F9F1FF;
						font: normal normal normal 45px/1 FontAwesome;
						
				}
			.graph {
			font-size: 16px;
			background: #F9F1FF;
			height:auto;
			width:90%;
			-moz-border-radius: 25px;
			border-radius: 25px ;
			display: inline-block;
			margin-left:auto;
			margin-right:auto;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}
			
			.graph:hover {
			background-color:#FFF;
			width:95%;
			height:auto;
			-moz-border-radius:0px;
			border-radius: 0px;
			font-size:17px;
			}
			.menu {
			position: relative;
			opacity:0.5;
			}
			.menu:hover {
			opacity:1;
			}
			.menu ul {
			list-style: none;
			margin: 0;
			padding: 0;
			}
			
			.menu ul li {
			display: block;
			float: left;
			list-style: none;
			margin: 0;
			padding: 0;
			position: relative;
			}
			.menu ul li a {
			display: block;
			padding: 3px 8px;
			text-decoration: none;
			}
			.menu ul li a:hover {
			background: #50147A;
			opacity:1.0;
			}
			.menu ul li a.active, .menu ul li a.active:hover {
			background: #000;
			}
			
			.pray{
			position: fixed;
			float: left;
			margin-left: -190px;
			bottom: 15px;
			left: 0;
			background: white;
			-webkit-animation:bounceIn 2s;
			-webkit-transition: all 0.2s ease-out;
			-moz-transition: all 0.2s ease-out;
			-o-transition: all 0.2s ease-out;
			transition: all 0.2s ease-out;
			}
			.pray:hover{
			margin-left: 0px;
			background: #F9F1FF;
			}
		</style>
        <?php include_once("../include/all_header.php") ?>
		
	</head>
	
	<body dir="rtl" bgcolor="#333333">
		
        <?php include_once("../include/all_body.php") ?>
		
        <table   width="95%" align="center"  >
			<tr>
				<td><div   class="header1" dir="rtl" >
					<div class="row" dir="rtl">
						<div class="col-xs-12 col-sm-4">
							
							<div class="menu" align="left">
								<ul>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_facebook"><img alt="تابعنا على الفيسبوك" src="../up/s/fb.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_youtube"><img alt="قناتنا على اليوتيوب" src="../up/s/y.png" /></a></li>
									<li><a target="_blank" rel="nofollow" href="http://bit.ly/pallap_twitter"><img alt="تابعنا على تويتر" src="../up/s/t.png" /></a></li>
								</ul>
							</div>
							
						</div>
						<div class="col-xs-12 col-sm-4"  >
							<div class="logo">
								<center>
									<a href="/" title="الى الرئيسية"> <img id="logo" src="../up/logo-color.png" alt="pallap بال لاب" /></a>
								</center>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="text-right text-muted">
								<h3> صفحة إبدأ  </h3>
							</div>
						</div>
					</div>
				</div></td>
			</tr>
			<tr>
				<td><div id="container">
					<div class="row" dir="rtl" style="font-family:Tahoma;">
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title"> مواقع تـهـمـــك </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/G_Translate_" title="ترجمة جوجل" target="_blank" rel="nofollow"> ترجمة - جوجل</a></li>
										<li><a href="http://bit.ly/webteb_com" title="وقع طبي وصحة شامل" target="_blank"> ويب طب </a></li>
										<li><a href="http://mawdoo3.com" title="موسوعة,عربية,شاملة,موضوع" target="_blank"> موضوع.كوم  </a></li>
										<li><a href="http://bit.ly/aliqtisadi" title="الأولى للأعمال وريادة الشباب في فلسطين" target="_blank">الاقتصادي </a></li>									
										<li><a href="http://bit.ly/arageek_com" title="منصـة إعلاميـة تثقيفيــة رقميــة شامــلة" target="_blank" rel="nofollow">اراجيك</a> </li>
										<li><a href="http://bit.ly/akhawat_islamway" title="اضخم منتدى للأخوات إسلامي" target="_blank">اخوات طريق الإسلام</a></li>
										<li><a href="http://www.muslmh.com" title=" شبكة أنا مسلمة" target="_blank"> شبكة أنا مسلمة</a></li>										
										<li><a href="http://bit.ly/aitnews_com" title=" البوابة العربية للأخبار التقنية " target="_blank" rel="nofollow"> البوابة للأخبار التقنية</a></li>
										<li><a href="http://pallap.com/m/2010/03/speed-net-test" title="سرعة اتصالك بالإنترنت الحقيقية" target="_blank">قياس سرعة الانترنت</a></li>

										
									</ul>
								</div>
							</div>
							<br>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/koooooora" title="اضخم موقع رياضي عربي" target="_blank" rel="nofollow">كوووورة</a></li>
										<li><a href="http://www.yallakora.com/" title=" موقع رياضي" target="_blank" rel="nofollow">يلا كورة</a></li>
										<li><a href="http://bit.ly/1lEw1gu" title="موقع رياضي متخصص في اخبار كرة القدم العربية والعالمية" target="_blank" rel="nofollow"> هاي كورة</a></li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title"> إسلاميات </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/mp3Quran" title="المكتبة الصوتية للقرآن الكريم mp3 تضم عدد كبير من القراء و بعدة روايات و بعدة لغات , مع روابط تحميل مباشرة " target="_blank">القرآن الكريم mp3</a></li>
										<li><a href="http://bit.ly/islamway" title="موقع طريق الاسلام" target="_blank">طريق الإسلام</a></li>
										<li><a href="http://bit.ly/saaid_net" title="موقع صيد الفوائد" target="_blank">صيد الفوائد</a></li>
										<li><a href="http://bit.ly/wathakker" title="موقع وذكّر" target="_blank"> وذكـر</a></li>
										<li><a href="http://bit.ly/islamqa_info" title="موقع الإسلام سؤال وجواب موقع دعوي، علمي تربوي يهدف إلى تقديم الفتاوى والإجابات العلمية المؤصلة" target="_blank" >الإسلام س ج</a></li>
										<li><a href="http://bit.ly/islamicfinder" title="IslamicFinder" target="_blank">الباحث الاسلامي</a></li>
										<li><a href="http://bit.ly/islamweb_net" title="موقع الشبكة الإسلامية" target="_blank">إسلام ويب</a></li>
										<li><a href="http://bit.ly/emanway" title="موقع طريق الايمان" target="_blank" rel="nofollow">طريق الايمان</a></li>
										<li><a href="http://bit.ly/dorar_net" title="لفحص صحة أي حديث " target="_blank" >الدرر السنية</a></li>
										<li><a href="http://bit.ly/quran-m_com" title="موسوعة الاعجاز العلمي في القرآن والسنة" target="_blank">موسوعة الاعجاز العلمي</a></li>
										<li><a href="http://bit.ly/almoslim_net" title="موقع المسلم" target="_blank">موقع المـسلم</a></li>
										<li><a href="http://www.nabulsi.com" title="موسوعة النابلسي للعلوم الإسلامية" target="_blank">موسوعة النابلسي</a></li>
										<li><a href="http://bit.ly/islamstory_com" title="موقع قصة الاسلام بإشراف الدكتور راغب السرجاني" target="_blank">قـصـة الإسـلام</a></li>
				


									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h3 class="panel-title"> فلسطينيات </h3>
							<div class="panell">
								<div class="graph">
                                    <ul>
										<li><a href="http://bit.ly/safa_ps" title="وكالة االصحافة الفلسطينية - صفا" target="_blank" rel="nofollow">وكالة صفا</a></li>
<li><a href="http://bit.ly/qudsn_ps" title="موقع شبكة قدس الاخبارية" target="_blank" rel="nofollow">شبكة قدس</a></li>
										<li><a href="http://bit.ly/alresalah" title="صحيفة الرسالة" target="_blank" rel="nofollow">الرسالة نت </a></li>										
										<li><a href="http://bit.ly/alwatanvoice_com" title="دنيا الوطن صحيفة" target="_blank" rel="nofollow">دنيا الوطن</a></li>
										<li><a href="http://bit.ly/palinfo_com" title=" المركز الفلسطيني للإعلام" target="_blank"> المركز الفلسطيني للإعلام</a></li>
										<li><a href="http://bit.ly/paltimes_net" title="فلسطين الآن | بوابتك إلى الحقيقة" target="_blank">فلسطين الآن</a></li>
										<li><a href="http://bit.ly/maannews_net" title="وكالة معاً الاخبارية" target="_blank" rel="nofollow">وكالة معا</a></li>
										
										<li><a href="http://bit.ly/felesteen_ps" title="موقع صحيفة فلسطين" target="_blank"> فلسطين أون لاين</a></li>
										<li><a href="http://bit.ly/paltoday_ps" title="فلسطين اليوم" target="_blank" rel="nofollow">وكالة فلسطين اليوم </a></li>
										<li><a href="http://bit.ly/paldf_net" title="اضخم منتدى فلسطيني" target="_blank">فلسطين . نت  </a></li>										
										<li><a href="http://bit.ly/pal_stu" title="منتديات ملتقى طلاب فلسطين" target="_blank">ملتقى طلاب فلسطين</a></li>										
										<li><a href="http://bit.ly/Pal_Remembered" title="موقع فلسطين في الذاكرة يحوي معلومات عن جميع المدن والقرى الفلسطينية" target="_blank" rel="nofollow">فلسطين في الذاكرة</a></li>
										<li><a href="https://shobiddak.com" title=" شو بدك من فلسطين" target="_blank" rel="nofollow">شو بدك من فلسطين</a></li>
										<li><a href="http://bit.ly/palweather_ps" title="طقس فلسطين" target="_blank" rel="nofollow">طقس فلسطين</a> </li>
										


									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 btn-group-vertical text-right "  style="font-family:KufiArabic;">

						<div class="row">
							<a class="btn btn-link col-sm-4 gray" href="http://bit.ly/youtube_com1" title="يوتيوب " target="_blank" rel="nofollow">
							<i class="fa-youtube logo_top_visit"></i><div class="txt_top_visit">يوتيوب </div></a>
																	
							<a class="btn btn-link col-sm-4" href="http://bit.ly/facebook_com1" title="حسابك في الفيسبوك" target="_blank" rel="nofollow">
							<i class="fa-facebook logo_top_visit" style="color:#4267b2;"></i><div class="txt_top_visit">فيسبوك </div></a>
														
							<a class="btn btn-link col-sm-4" href="http://bit.ly/google_com1" title="جـوجــل"  target="_blank" rel="nofollow">
							<i class="fa-google logo_top_visit"></i><div class="txt_top_visit">جــوجــل </div></a>						

							
  						</div>

						  <div class="row">
						  <a class="btn btn-link col-sm-4" href="http://bit.ly/twitter_com1" title="تويتر " target="_blank" rel="nofollow">
						  <i class="fa-twitter logo_top_visit" style="color:#1da1f2;"></i><div class="txt_top_visit">تويتر </div></a>

						  <a class="btn btn-link col-sm-4" href="http://bit.ly/ar_wikipedia_org" title="wikipedia موسوعة ويكيبيديا" target="_blank" rel="nofollow">
						  <i class="logo_top_visit" style="color:black;" >W</i><div class="txt_top_visit">ويكيبيديا </div></a>

						  <a class="btn btn-link col-sm-4" href="http://bit.ly/soundcloud_com1" title="ساوند كلاود"  target="_blank" rel="nofollow">
						  <i class="fa-soundcloud logo_top_visit" style="color:#f50;"></i><div class="txt_top_visit">SoundCloud </div></a>				
						  
						  </div>

							
						<div class="row">
						<a class="btn btn-link col-sm-4 test" href="http://bit.ly/outlook_com1" title="الدخول الى  ايميل"  target="_blank" rel="nofollow">
						<i class="fa-windows logo_top_visit"  style="color:#0067b8;"></i><div class="txt_top_visit">Outlook </div></a>				
						
						<a class="btn btn-link col-sm-4" href="http://bit.ly/linkedin_com1" title="www.linkedin.com" target="_blank" rel="nofollow">
						<i class="fa-linkedin logo_top_visit"  style="color:#0084bf;"></i><div class="txt_top_visit">LinkedIn </div></a>				

						<a class="btn btn-link col-sm-4"  href="http://bing.com" title="محرك بحث بنج " target="_blank" rel="nofollow">
						<i class="logo_top_visit"><img alt=" محرك البحث التابع لمايكروسوف بنج - لايف " src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Bing_favicon.svg/48px-Bing_favicon.svg.png"></i>
						<div class="txt_top_visit">Bing </div></a>				
						</div>

						<div class="row">
						<a class="btn btn-link col-sm-4" href="http://bit.ly/flickr_com1" title="فليكر "   target="_blank" rel="nofollow">
						<i class="fa-flickr logo_top_visit"  style="color:#2d1152;"></i><div class="txt_top_visit">فليكر </div></a>	

						<a class="btn btn-link col-sm-4" href="http://yahoo.com" title="ياهو "   target="_blank" rel="nofollow">
						<i class="fa-yahoo logo_top_visit"  style="color:#2d1152;"></i><div class="txt_top_visit">ياهو </div></a>	
						
						<a class="btn btn-link col-sm-4" href="https://web.whatsapp.com/" title="واتساب  WhatsApp "   target="_blank" rel="nofollow">
						<i class="fa-whatsapp logo_top_visit"  style="color:#43d854;"></i><div class="txt_top_visit">واتساب </div></a>	
			
						<a class="btn btn-link col-sm-4"  href="http://bit.ly/aljazeera_net" title="موقع قناة الجزيرة"  target="_blank" rel="nofollow">
						<i class="logo_top_visit"><img alt="قناة الجزيرة  " src="https://upload.wikimedia.org/wikipedia/en/thumb/7/71/Aljazeera.svg/48px-Aljazeera.svg.png"></i>
						<div class="txt_top_visit">الجزيرة نت  </div></a>

						</div>	


						<div class="row">
						<a  class="btn" href="https://www.facebook.com/pallap2" target="_blank"  rel="nofollow" title="إقترح إضافة موقع للصفحة"> شاركنا برأيك ○ أو اقترح اضافة موفع ♥   </a>
						
						</div>						

						

					
				</div>
				<?PHP
					$footer='on';
					include_once("../include/all_footer.php");

				?>
				</div></td>
		</tr>
	</table>
	

			
	<div class="pray">
		<iframe src="http://www.prayer-times.info/show_prayertimes.php?city_link=alquds&box_style=2" frameborder="0" width="226" height="475" scrolling="off"></iframe>
		
	</div>	
	
</body>
</html>