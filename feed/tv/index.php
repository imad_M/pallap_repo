<?php
	header("Content-type: text/xml"); 
	
	require_once '../../include/config.php';
	
	
	
	$xmlns = 'xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:atom="http://www.w3.org/2005/Atom" 
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"';
	
	
	$q2 = "SELECT * FROM `pallapc_main`.`main_pages` WHERE id_main=1 ";
	$g = mysqli_query($dbC,$q2);
	
	while($dd = mysqli_fetch_array($g,MYSQLI_BOTH)){
		
		$title_main		= $dd['title_main']; 
		$description_main	=  htmlspecialchars($dd['description_main']);
	}
	
	
	echo "<?xml version='1.0' encoding='UTF-8'?> 
	<rss version='2.0' $xmlns >
	<channel>
	<title>$title_main</title>
	<link>https://pallap.com/tv.php</link>
	<description>$description_main</description>
	<language>ar</language>"; 
	
	
	$query = "SELECT id, head_title, head_description, datechange, link FROM `pallapc_main`.`tv`  WHERE type=1 ORDER BY id desc";
	$getBlogDisplay = mysqli_query($dbC,$query);
	
	while($data = mysqli_fetch_array($getBlogDisplay,MYSQLI_BOTH)){
		$title			= $data['head_title']; 
		$link			= $data['link'];
		$description	=  htmlspecialchars($data['head_description']);
		
		echo "<item> 
		<title>$title</title>
		<link>https://pallap.com/tv.php?i=$link</link>
		<description>$description</description>
		</item>"; 
	} 
	echo "</channel></rss>";
?>		