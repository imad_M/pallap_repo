<?php
header('Content-type: application/xml');
require_once 'rss_feed.php'; // configure appropriately
 
// set more namespaces if you need them
$xmlns = 'xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:atom="http://www.w3.org/2005/Atom" 
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"';
 
// configure appropriately
$a_channel = array(
  "title" => "live stream TV pallap",
  "link" => "http://www.pallap.com",
  "description" => "البث المباشر للقنوات العربية ، شاهد القنوات التلفزيونية ، تلفزيون على الانترنت",
  "language" => "ar",
  "image_title" => "pallap",
  "image_link" => "http://www.pallap.com",
  "image_url" => "http://pallap.com/up/s/rss.png",
);
$site_url = 'http://www.pallap.com'; // configure appropriately
$site_name = 'البث المباشر للقنوات العربية ، شاهد القنوات التلفزيونية ، تلفزيون على الانترنت'; // configure appropriately
 
$rss = new rss_feed($xmlns, $a_channel, $site_url, $site_name);
echo $rss->create_feed();
?>