<? session_start();
if(session_is_registered('admin102')){
	require_once('config.php');
	} 
else	{
?>
<script type="text/javascript">
<!--
window.location = "login.php"
//-->
</script>


<? }

	date_default_timezone_set('Asia/Jerusalem');
	$date = date("y-m-d");

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form2")) {
  $insertSQL = sprintf("INSERT INTO `pallapc_main`.`questions`(id_q, question, image_url, img_alt, type, date, no_correct, no_false) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_q'], "int"),
                       GetSQLValueString($_POST['question'], "text"),
                       GetSQLValueString($_POST['image_url'], "text"),
                       GetSQLValueString($_POST['img_alt'], "text"),
                       GetSQLValueString($_POST['type'], "int"),
                       GetSQLValueString($_POST['date'], "date"),
                       GetSQLValueString($_POST['no_correct'], "int"),
                       GetSQLValueString($_POST['no_false'], "int"));

  mysql_select_db($database_adminC, $adminC);
  $Result1 = mysql_query($insertSQL, $adminC) or die(mysql_error());
  $qid = mysql_insert_id();
  $insertGoTo = "exames_add_answer.php?qid=$qid";
  header(sprintf("Location: %s", $insertGoTo));
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Admin</title>
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

 
<body bgcolor="#666666"  dir="ltr"  >
<table width="90%"  align="center" bgcolor="#FFFFFF" class="table" dir="ltr">
  <tr><td colspan="2" >  <? include("header.php") ?></td></tr>
  
    <td width="220" bgcolor="#FFCCFF"><? include 'menu.php'; ?> </td>
    <td>

    <h1>Add question </h1>
	<p>&nbsp;</p>
	<p>
	  
	  
	  
<form method="post" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data" name="form2">
<table width="90%" border="0"  cellpadding="5" cellspacing="5" bgcolor="#DDDDDD" >

  <tr onMouseOver="bgColor='#cccccc'" onMouseOut="bgColor='#DDDDDD'" >
    <td >question</td> 
    <td><textarea name="question"  cols="60" rows="5" dir="rtl"></textarea></td>   
  </tr>

	<tr onMouseOver="bgColor='#cccccc'" onMouseOut="bgColor='#DDDDDD'">
	<td>Image Link - <b><br>start with <font color="#FF0000"> http://</font></b></td> 
	<td><input name="image_url" type="text" dir="ltr" id="link" size="60"></td>   
	</tr>

	<tr align="left" onMouseOver="bgColor='#cccccc'" onMouseOut="bgColor='#DDDDDD'">
       <td>img_alt</td>
       <td><input name="img_alt" type="text" dir="rtl" size="50"></td>
	</tr>
	<tr align="left" onMouseOver="bgColor='#cccccc'" onMouseOut="bgColor='#DDDDDD'">
       <td>type</td>
       <td><select name="type" dir="rtl">
         <option value="1">خصوصي</option>
         <option value="2">عمومي</option>
       </select></td>
	</tr>

</table>
<br/>

<input type="submit" name="submit" value="     Add      " onclick='this.value="Please wait...";' />



  <input type="hidden" name="id_q" value="">
  <input type="hidden" name="no_correct" value="0">
  <input type="hidden" name="no_false" value="0">
  <input type="hidden" name="date" value="<? echo $date; ?>">
  <input type="hidden" name="MM_insert" value="form2">
  <input name="qid" type="hidden" id="qid" value="<? echo $qid; ?>">
  
</form>
    </p>

   
	</td>
  </tr>
</table>
<div class="footer">
  <? require_once('footer.php'); ?> </div>

</body>
</html>