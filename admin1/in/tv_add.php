<?php
	if(!isset($_SESSION['user_id'])){
		header("location:login.php");
	}
?>

<?php
	$change="";
	if(isset($_POST['submit'])) {

		$head_title=$_POST['head_title'];
		$head_description= mysqli_real_escape_string ($conn ,$_POST['head_description']);
		$title= mysqli_real_escape_string ($conn,$_POST['title']);
		$description= mysqli_real_escape_string ($conn,$_POST['description']);
		$code= mysqli_real_escape_string ($conn,$_POST['code']);
		$on_head= mysqli_real_escape_string ($conn,$_POST['on_head']);
		$more= mysqli_real_escape_string ($conn,$_POST['more']);
		$views=$_POST['views'];
		$img= mysqli_real_escape_string ($conn,$_POST['img']);
		$img_alt= mysqli_real_escape_string ($conn,$_POST['img_alt']);
		$img_big= mysqli_real_escape_string ($conn,$_POST['img_big']);
		$like=$_POST['likes'];
		$dislike=$_POST['dislike'];
		$type=$_POST['type'];
		$link_p= mysqli_real_escape_string ($conn,$_POST['link']);

		$datecreat = $date;

		$sql_c="SELECT id FROM `pallapc_main`.`tv` where `link`='$link_p' ";
		$result_c=mysqli_query($conn ,$sql_c);
		$rowcount_c=mysqli_num_rows($result_c);
		if($rowcount_c!=0){
			echo "<b><center>ERROR , the link ($link_p) was used before</center> </b>";
			}else{

			$sql="INSERT INTO  `pallapc_main`.`tv` (
			`head_title` ,
			`head_description` ,
			`title` ,
			`description` ,
			`on_head` ,
			`more` ,
			`code` ,
			`views` ,
			`img` ,
			`img_alt` ,
			`img_big` ,
			`likes` ,
			`dislike` ,
			`type` ,
			`date` ,
			`datechange` ,
			`link`
			)
			VALUES (
			'$head_title','$head_description','$title','$description', '$on_head', '$more', '$code', '$views', '$img', '$img_alt', '$img_big', '$like', '$dislike', '$type', '$datecreat', '$datecreat', '$link_p')";

			$result=mysqli_query($conn, $sql);

			if($result){
				$change=' TV  Uploaded Successfully!';
			$color='alert alert-success';	}
			else	{
				echo $head_description .'<br>'.$head_title;
				$change=" E r r o R mysql_query";
			$color='bg-danger';	}

		}
	}

?>
<p role="alert" class="<?PHP echo $color;?>"> <?php echo $change; ?> </p>

<h3>Add TV</h3>

<form method="post" action="" enctype="multipart/form-data" name="form1" role="form">
	<table width="90%" class="table table-hover" >
		<tr align="left">
			<tr align="left" class="info">
				<td>&nbsp;</td>
				<td><input name="link" type="text" dir="ltr" size="20" required  placeholder="Link" ></td>
			</tr>
			<tr class="info">
				<td>&nbsp;</td>
				<td><input name="head_title" type="text" dir="ltr" size="50" required placeholder="Head Title "></td>
			</tr>
			<tr class="info">
				<td >Head description</td>
				<td><textarea required name="head_description" cols="30" rows="2" dir="rtl"></textarea></td>
			</tr>
			<tr class="info">
				<td >more code on_head</td>
				<td><textarea class="form-control"  name="on_head" cols="50" rows="1" dir="ltr"></textarea></td>
			</tr>
			<tr>
				<td >more info about (mini font)</td>
				<td><textarea class="form-control"  name="more" cols="50" rows="2" dir="rtl"></textarea></td>
			</tr>
			<tr>
				<td >&nbsp;</td>
				<td><input name="title" required type="text" dir="ltr"  size="50" placeholder="Short Title "></td>
			</tr>
			<tr>
				<td >Short main description </td>
				<td><textarea name="description" required cols="50" rows="2" dir="rtl"></textarea></td>
			</tr>
			<tr class="danger" >
				<td >code video</td>
				<td><textarea class="form-control"  name="code"  cols="50" rows="5" dir="ltr" required ></textarea></td>
			</tr>
			<tr >
				<td>Image Link </td>
				<td><input name="img" type="text" dir="ltr"  size="60" required placeholder="Start with http:// "></td>
			</tr>
			<tr align="left" >
				<td>img_big</td>
				<td><input name="img_big" type="text" dir="rtl" size="40" placeholder="img_big"></td>
			</tr>
			<tr align="left" >
				<td>img_alt</td>
				<td><input name="img_alt" type="text" dir="rtl" size="40" placeholder="img_alt"></td>
			</tr>
		</table>
		<br/>
		<input type="submit" class="addbtn btn btn-primary btn-block" name="submit" value=" ...........  Add  ............  " onclick='this.value="Please wait...";' />
		<input type="hidden" value="500" name="views" />
		<input type="hidden" value="1" name="like" />
		<input type="hidden" value="1" name="dislike" />
		<input type="hidden" value="1" name="type" />
	</form>
</p>
