<?php
	if(!isset($_SESSION['user_id'])){
		header("location:login.php");
	}
	
	//////////// Delete all last 365 DAYS
    if(isset($_GET["DELETE5"]) && $_GET["DELETE5"]=="ok"){
		$sqld=" DELETE FROM `pallapc_main`.`report`
		WHERE date <= DATE(DATE_SUB(NOW(), INTERVAL 365 DAY)) ";
		$resultd=mysqli_query($conn,$sqld);
		if($resultd){
			echo "<h1><p role='alert' class='alert alert-success'>&#10004;Reports before 365 days deleted</p></h1>";
		?>
		<script type="text/javascript">
			<!--
			window.location = "<?php echo 'index.php?sec=reports';?>"
			//-->
		</script>
		<?php
			}else{
			echo "Error in delteing before 365 days" ;
		} //////////// End Delete all last 365 DAYS
		
		}else{
		
		$sq="SELECT id_r FROM `pallapc_main`.`report` ";
		$r_c=mysqli_query($conn ,$sq);
		$row_c=mysqli_num_rows($r_c);
		if($row_c==0){
			echo "no reports to show"   ;
			}else{
			
		?>
		
		
		<h2> Reports - Last 7 Days</h2>
		
		
		<div align="center" >
            <table width="365%" class="table table-hover">
				<thead>
					<tr>
						<th  align="center" class="col-md-1">#</td>
						<th  align="left" class="col-md-2" ><a href="index.php?sec=reports&orderby=title">Title</a></td>
						<th  class="col-md-1" align="center"><a href="index.php?sec=reports&orderby=date">Date</a></td>
						<th  class="col-md-1" align="center">Time</td>
						<th  class="col-md-2" align="center"><a href="index.php?sec=reports&orderby=OS">OS</a></td>
						<th  class="col-md-1" align="center"><a href="index.php?sec=reports&orderby=ip">ip</a></td>
						<th  class="col-md-2" align="center"><a href="index.php?sec=reports&orderby=browser">Browser</a></td>
						<th class="col-md-1"></td>
					</tr>
				</thead>
				<?php
					$no_q = 1;
					
					if(isset($_GET["orderby"]) && $_GET["orderby"]=="date"){
							$sql2="SELECT * FROM `pallapc_main`.`report` where date >= DATE_SUB(NOW(), INTERVAL 7 DAY) order by date desc";
						}elseif(isset($_GET["orderby"]) && $_GET["orderby"]=="OS"){
							$sql2="SELECT * FROM `pallapc_main`.`report` where date >= DATE_SUB(NOW(), INTERVAL 7 DAY) order by OS desc ";
						} elseif(isset($_GET["orderby"]) && $_GET["orderby"]=="ip"){
							$sql2="SELECT * FROM `pallapc_main`.`report` where date >= DATE_SUB(NOW(), INTERVAL 7 DAY) order by ip desc ";
						}elseif(isset($_GET["orderby"]) && $_GET["orderby"]=="browser"){
							$sql2="SELECT * FROM `pallapc_main`.`report` where date >= DATE_SUB(NOW(), INTERVAL 7 DAY) order by browser desc ";
						}elseif(isset($_GET["orderby"]) && $_GET["orderby"]=="title"){
							$sql2="SELECT * FROM `pallapc_main`.`report` where date >= DATE_SUB(NOW(), INTERVAL 7 DAY) order by title desc ";
						} else{
							$sql2="SELECT * FROM `pallapc_main`.`report`
							where date >= DATE_SUB(NOW(), INTERVAL 7 DAY)
							order by id_r desc  ";
					}
					
                    /// No. of all reports
                    $sql_all="SELECT id_r FROM `pallapc_main`.`report`";
					$result_all=mysqli_query($conn,$sql_all);
                    $rowcount=mysqli_num_rows($result_all);
					echo $rowcount;
                    /// END No. of all reports
					
					
					$my_date = date("Y-m-d");
					$result2=mysqli_query($conn,$sql2);
					
					
					while($rows=mysqli_fetch_array($result2)){
						$id_page=$rows['id_page'];
						$date=$rows['date'];
						$time=$rows['time'];
						$OS=$rows['OS'];
						$ip=$rows['ip'];
						$browser=$rows['browser'];
						$id_r=$rows['id_r'];
						
						if($date==$my_date){
							$color='class="danger"';
							}else{$color='';
						}
						
						
						$sql3="SELECT * FROM `pallapc_main`.`tv` where id=$id_page ";
						$result3=mysqli_query($conn,$sql3);
						while($rows2=mysqli_fetch_array($result3)){
							$type=$rows2['type'];
							$title=$rows2['title'];
							$img=$rows2['img'];
							$link=$rows2['link'];
						}
						
						
						if($type==1){
							$pg='tv';
							}else{
							$pg='radio';
						}
						
						
						
						if($browser=='Chrome'){
							$browser_img=" <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Google_Chrome_icon_%28September_2014%29.svg/50px-Google_Chrome_icon_%28September_2014%29.svg.png' alt='Chrome' /> ";
							$browser="";
							}elseif($browser=='Firefox'){
							$browser_img=" <img   height='60' src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Firefox_logo%2C_2019.svg/50px-Firefox_logo%2C_2019.svg.png' alt='Firefox' /> ";
							$browser="";
							}elseif($browser=='Safari'){
							$browser_img=" <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Safari_browser_logo.svg/50px-Safari_browser_logo.svg.png' alt='Safari' /> ";
							$browser="";
							}elseif($browser=='Opera'){
							$browser_img=" <img  height='50' src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Opera_browser_logo_2013.png/365px-Opera_browser_logo_2013.png' alt='Opera' /> ";
							$browser="";
							}else{
							$browser_img="";
						}
					?>
					<tbody>
						<tr <?php echo $color; ?> >
							<td align="center"><?php echo $no_q; ?></td>
							<td><a class="text-left btn btn-block btn-link" target="_blank" href="https://pallap.com/<?php echo $pg; ?>.php?i=<?PHP echo $link;  ?>"><img class="text-left"  height="50" src="<?php echo $img; ?>"  />&nbsp;<?php echo $title; ?> </a></td>
							<td align="center"><?php echo $date; ?></td>
							<td align="center"><?php echo $time; ?></td>
							<td align="center"><?php echo $OS; ?></td>
							<td align="center"><?php echo $ip; ?></td>
							<td align="center"><?php echo $browser_img. " " .$browser ; ?></td>
							
							<td align="center" ><a class="btn  btn-danger" href="index.php?sec=report_delete&id=<?php echo $id_r; ?>" onClick="return confirm('Are you shure you want to delete this report?')"><strong>  &#10006; </strong></a></td>
						</tr>
						
						<?php $no_q = $no_q + 1;
						} ?>
				</tbody>
			</table>
			
			
		</div> <br>
		<!---------  Delete all last 365 DAYS -->
		<br>
		<a href="index.php?sec=reports&DELETE5=ok" class="btn  btn-danger">Delete Reports before 365 days </a>
		<br>
		<!---------  Delete all last 365 DAYS -->
		
		<?PHP
			
			
			
			
		} // delete5
		
		
		
	} // no reports to show
?>