<?php
session_start();

if(isset($_SESSION['user_id'])){
	
	require_once('Connections/pallapc_main.php');
	$admin_name = $_SESSION['user_id'];
	date_default_timezone_set("Asia/Jerusalem"); 
	$date = date('Y-m-d'); 
	$day = date("l"); 




	// ADD update last login

	  include 'in/header.php';

	if(!isset($_GET["sec"]) || $_GET["sec"]=="dashboard")
		include './in/dashboard.php';
	else if($_GET["sec"]=="tv")
		include './in/tv.php';
	else if($_GET["sec"]=="tv_add")
		include './in/tv_add.php';
	else if($_GET["sec"]=="tv_delete")
		include 'in/tv_delete.php';
	else if($_GET["sec"]=="tv_edit")
		include './in/tv_edit.php';
	else if($_GET["sec"]=="radio")
		include './in/radio.php';
	else if($_GET["sec"]=="admin")
		include './in/admin.php';
	else if($_GET["sec"]=="radio_edit")
		include './in/radio_edit.php';
	else if($_GET["sec"]=="radio_add")
		include './in/radio_add.php';
	else if($_GET["sec"]=="radio_delete")
		include './in/radio_delete.php';
	else if($_GET["sec"]=="main_edit")
		include './in/main_edit.php';
	else if($_GET["sec"]=="reports")
		include './in/reports.php';
	else if($_GET["sec"]=="report_delete")
		include './in/report_delete.php';
	else if($_GET["sec"]=="tv_cat")
		include './in/cat.php';
	else
		include './in/dashboard.php';
	  include './in/footer.php';
} else{
	echo "<h1>Sorry, You must be logged in to access this page</h1>";
	?>
    <meta http-equiv="Refresh" content="1; url=/admin1/login.php">
    <br />
    You will be redirect to 
    <a href="login.php"> Login page</a>
<?php	
}
?>
